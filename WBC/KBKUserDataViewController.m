//
//  KBKUserDataViewController.m
//  WBC
//
//  Created by Kevin on 5/31/14.
//  Copyright (c) 2014 Boardgame Players Association. All rights reserved.
//

#import "KBKUserDataViewController.h"

@implementation KBKUserDataViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}

- (void)viewDidLoad
{
  [super viewDidLoad];
  
  [self.navigationItem setTitle:@"My WBC Data"];

  // set navigation buttons
  [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"BarButtonIconQuestion"] style:UIBarButtonItemStylePlain target:self action:@selector(openHelp)]];

  [self controlValueChanged:nil];
}



- (void)didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (IBAction)controlValueChanged:(id)sender {
  NSInteger current = _segmentedControl.selectedSegmentIndex;
  
  UIViewController *newView;
  if (current == 0) {
    newView = [self.storyboard instantiateViewControllerWithIdentifier:@"UserEventsViewController"];
  } else if (current == 1) {
    newView = [self.storyboard instantiateViewControllerWithIdentifier:@"UserFinishesViewController"];
  } else if (current == 2) {
    newView = [self.storyboard instantiateViewControllerWithIdentifier:@"UserNotesViewController"];
  }
  
  [self addChildViewController:newView];
  [self.viewContainer addSubview:newView.view];
  
  [newView didMoveToParentViewController:self];
  
  
}

-(IBAction)openHelp
{
  UIViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"HelpViewController"];
  [self.navigationController pushViewController:newView animated:YES];
}
@end
