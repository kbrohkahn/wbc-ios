//
//  KBKSchedulePageViewController.m
//  WBC
//
//  Created by Kevin on 5/29/14.
//  Copyright (c) 2014 Boardgame Players Association. All rights reserved.
//

#import "KBKScheduleTableViewController.h"
#import "KBKScheduleViewController.h"
#import "KBKWBCData.h"
#import "KBKEvent.h"
#import "KBKTournament.h"
#import "KBKEventGroup.h"
#import "KBKTournamentViewController.h"
#import "KBKSearchTableViewController.h"

@interface KBKScheduleViewController ()

@end

@implementation KBKScheduleViewController


- (void)viewDidLoad
{
  [super viewDidLoad];

  // set up navigation bar buttons
  NSMutableArray *rightButtons = [[NSMutableArray alloc]init];
  [rightButtons addObject:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(openSearch)]];
  [rightButtons addObject:[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"BarButtonIconQuestion"] style:UIBarButtonItemStylePlain target:self action:@selector(openHelp)]];
  [self.navigationItem setRightBarButtonItems:rightButtons];

  int day = [KBKWBCData getDay];
  if (day == -1) {
    day = 0;
  }


  // set up page view controller
  NSString *title = [[KBKWBCData displayDayStrings] objectAtIndex:day];
  [self.navigationController.navigationBar.topItem setTitle:[@"WBC " stringByAppendingString:title]];

  self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
  self.pageViewController.dataSource = self;
  
  KBKScheduleTableViewController *startingViewController = [self viewControllerAtIndex:day];
  NSArray *viewControllers = @[startingViewController];
  [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
  
  // change the size of page view controller
  self.pageViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
  
  [self addChildViewController:_pageViewController];
  [self.view addSubview:_pageViewController.view];
  [self.pageViewController didMoveToParentViewController:self];
  
}

-(UIViewController *) pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
  NSUInteger index = ((KBKScheduleTableViewController*) viewController).pageIndex;
  
  if ((index == 0) || (index == NSNotFound)) {
    return nil;
  }
  
  index--;
  return [self viewControllerAtIndex:index];
  
}

-(UIViewController *) pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
  NSUInteger index = ((KBKScheduleTableViewController*) viewController).pageIndex;
  
  if ((index+1 == [[KBKWBCData dayList] count]) || (index == NSNotFound)) {
    return nil;
  }
  
  index++;
  return [self viewControllerAtIndex:index];
  
}

-(KBKScheduleTableViewController *) viewControllerAtIndex:(NSUInteger)index
{
  if (([[KBKWBCData dayList] count] == 0) || (index >= [[KBKWBCData dayList] count])) {
    return nil;
  }
  
  KBKScheduleTableViewController *pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageContentController"];
  
  pageContentViewController.eventGroups = [[KBKWBCData dayList] objectAtIndex:index];
  pageContentViewController.pageIndex = index;
  
  return pageContentViewController;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}

- (void)didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

-(IBAction)openSearch
{
  UIViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchTableViewController"];
  [self.navigationController pushViewController:newView animated:YES];
}

-(IBAction)openHelp
{
  UIViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"HelpViewController"];
  [self.navigationController pushViewController:newView animated:YES];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
