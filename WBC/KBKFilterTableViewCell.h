//
//  KBKFilterTableViewCell.h
//  WBC
//
//  Created by Kevin on 6/21/14.
//  Copyright (c) 2014 Boardgame Players Association. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KBKFilterTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *itemLabel;

@end
