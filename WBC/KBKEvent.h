//
//  KBKEvent.h
//  WBC
//
//  Created by Kevin on 5/28/14.
//  Copyright (c) 2014 Boardgame Players Association. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KBKEvent : NSObject

//7/27/2013~9~18XX D1/1~~B~Demo~1~~Pierre LeBoeuf~Terrace 4~~
@property (nonatomic) NSString* eventID;
@property (nonatomic) int tournamentID;

@property (nonatomic) int day;
@property (nonatomic) int hour;

@property (strong, nonatomic) NSString *title;

@property (strong, nonatomic) NSString *eClass;
@property (strong, nonatomic) NSString *eFormat;
@property (nonatomic) BOOL qualify;

@property (nonatomic) double duration;
@property (nonatomic) BOOL continuous;
@property (nonatomic) double totalDuration;

@property (strong, nonatomic) NSString *location;

@property (nonatomic) BOOL starred;
@property (strong, nonatomic) NSString *note;

-(id) initWithValues:(NSString*) i tournament:(int) tID date:(int) dat hour:(int) h title:(NSString *)t class:(NSString *) cl format:(NSString*) f qualify:(BOOL) q duration:(double) dur continuous:(BOOL) co totalDuration:(double) td location:(NSString *) l;

@end
