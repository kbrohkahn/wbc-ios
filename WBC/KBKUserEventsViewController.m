//
//  KBKUserEventsViewController.m
//  WBC
//
//  Created by Kevin on 8/2/14.
//  Copyright (c) 2014 Boardgame Players Association. All rights reserved.
//

#import "KBKUserEventsViewController.h"
#import "KBKEvent.h"
#import "KBKEventGroup.h"
#import "KBKTournamentTableViewCell.h"
#import "KBKMapContainer.h"
#import "KBKTournament.h"
#import "KBKEventViewController.h"
#import "KBKAddEventController.h"
#import "KBKWBCData.h"

@implementation KBKUserEventsViewController

static NSMutableArray* events;
+ (NSMutableArray*) events
{ @synchronized(self) { return events; } }

- (void)viewDidLoad
{
  [super viewDidLoad];
  
  // load user events from daylist
  events = [[NSMutableArray alloc] init];
  NSArray * dayList = [KBKWBCData dayList];
  NSMutableArray *searchList;
  for (int i=0; i<[dayList count]; i++) {
    for (int j=1; j<[dayList[i] count]; j++) {
      searchList = ((KBKEventGroup *) dayList[i][j]).events;
      
      for (int k=0; k<[searchList count]; k++) {
        if (((KBKEvent *)searchList[k]).tournamentID == 0)
          [events addObject:((KBKEvent *)searchList[k])];
      }
    }
  }

  [_tournamentStar setBackgroundImage:[UIImage imageNamed:([self checkAllStar] ? @"StarOn" :@"StarOff")] forState:UIControlStateNormal];

}

-(void) viewDidAppear:(BOOL)animated
{
  [self.tableView reloadData];
  [_tournamentStar setBackgroundImage:[UIImage imageNamed:([self checkAllStar] ? @"StarOn" :@"StarOff")] forState:UIControlStateNormal];
}

-(BOOL) checkAllStar
{
  BOOL allStarred = YES;
  
  KBKEvent *event;
  for (int i=0; i<[events count]; i++)
  {
    event = events[i];
    if (!event.starred)
    {
      allStarred = NO;
      break;
    }
  }
  return allStarred;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  return [events count];
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  static NSString *simpleTableIdentifier = @"TournamentPortraitCell";
  KBKTournamentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
  
  if (cell == nil)
    cell = [[KBKTournamentTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
  
  KBKEvent *e =  [events objectAtIndex:indexPath.row];
  UIColor *textColor = [KBKWBCData getEventTextColor:e];
  UIFont *textFont = [KBKWBCData getEventTextFont:e];
  
  cell.titleLabel.text = e.title;
  [cell.titleLabel setTextColor:textColor];
  [cell.titleLabel setFont:textFont];
  
  cell.locationLabel.color = textColor;
  cell.locationLabel.text = e.location;
  [cell.locationLabel setTextColor:textColor];
  [cell.locationLabel setFont:textFont];
  
  cell.dayLabel.text = [[KBKWBCData displayDayStrings] objectAtIndex:e.day];
  [cell.dayLabel setTextColor:textColor];
  [cell.dayLabel setFont:textFont];
  
  cell.timeLabel.text = [NSString stringWithFormat:@"%li", (long)e.hour];
  [cell.timeLabel setTextColor:textColor];
  [cell.timeLabel setFont:textFont];
  
  cell.durationLabel.text = [NSString stringWithFormat:@"%2.2f", e.duration];
  [cell.durationLabel setTextColor:textColor];
  [cell.durationLabel setFont:textFont];
  
  cell.formatLabel.text = e.eFormat;
  [cell.formatLabel setTextColor:textColor];
  [cell.formatLabel setFont:textFont];
  
  cell.classLabel.text =  e.eClass;
  [cell.classLabel setTextColor:textColor];
  [cell.classLabel setFont:textFont];
  
  cell.starButton.tag =  indexPath.row;
  [cell.starButton setBackgroundImage:[UIImage imageNamed:(e.starred?@"StarOn":@"StarOff")] forState:UIControlStateNormal];
  
  UITapGestureRecognizer *singleFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                    action:@selector(didTapLabelWithGesture:)];
  [cell.locationLabel addGestureRecognizer:singleFingerTap];
  
  UILongPressGestureRecognizer *longPressListener = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressCell:)];
  [cell addGestureRecognizer:longPressListener];
  
  if ([cell isSelected])
    [cell setBackgroundColor:[[UIColor alloc] initWithRed:.9365f green:.75f blue:.75f alpha:1.0f]];
  else if ([cell isHighlighted])
    [cell setBackgroundColor:[[UIColor alloc] initWithRed:.0f green:.5f blue:1.0f alpha:1.0f]];
  else if (indexPath.row % 2 == 0)
    [cell setBackgroundColor:[KBKWBCData getOddCellColor:e]];
  else
    [cell setBackgroundColor:[KBKWBCData getEvenCellColor:e]];
  
  return cell;
}

- (IBAction)changeEventStar:(id)sender
{
  UIButton * starButton = ((UIButton *) sender);
  
  NSInteger tag = starButton.tag;
  
  KBKEvent *e = [events objectAtIndex:tag];
  e.starred = !e.starred;
  
  [starButton setBackgroundImage:[UIImage imageNamed:(e.starred?@"StarOn":@"StarOff")] forState:UIControlStateNormal];
  
  [KBKWBCData updateStarredEvent:e];
  
  BOOL allStarred = [self checkAllStar];
  [_tournamentStar setBackgroundImage:[UIImage imageNamed:(allStarred ? @"StarOn" :@"StarOff")] forState:UIControlStateNormal];
  [self.tableView reloadData];

}

- (void)didTapLabelWithGesture:(UITapGestureRecognizer *)tapGesture
{
  NSString * room = ((UILabel*)tapGesture.view).text;
  KBKMapContainer *map = [self.storyboard instantiateViewControllerWithIdentifier:@"MapContainer"];
  map.room = room;
  
  [self.navigationController pushViewController:map animated:YES];
}

-(IBAction)changeTournamentStar:(id)sender
{
  BOOL allStarred = [self checkAllStar];
  allStarred = !allStarred;
  
  [_tournamentStar setBackgroundImage:[UIImage imageNamed:(allStarred ? @"StarOn" :@"StarOff")] forState:UIControlStateNormal];
  
  KBKEvent *event;
  for (int i=0; i<[events count]; i++) {
    event = ((KBKEvent *) events[i]);
    
    if (event.starred != allStarred) {
      event.starred = allStarred;
      
      [KBKWBCData updateStarredEvent:event];
    }
  }
  [self.tableView reloadData];
}

-(IBAction) addEvent:(id)sender
{
  KBKAddEventController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"CreateEventController"];
  [self.navigationController pushViewController:newView animated:YES];
}

-(IBAction) deleteAll:(id)sender
{
  self.selectedEvent = -1;
  [self showDeleteAlert];
}

- (void)longPressCell:(UITapGestureRecognizer *)tapGesture
{  
  if (tapGesture.state == UIGestureRecognizerStateEnded)
  {
    // TODO
    //CGPoint touchPoint = [tapGesture locationInView:self.tableView];
    NSIndexPath* row = nil;//[self.tableView indexPathForRowAtPoint:touchPoint];
    
    if (row != nil) {
      self.deletingEvent = row.row;
      [self showDeleteAlert];
    }
  }
}

-(void) showDeleteAlert
{
  NSString *title;
  if (self.deletingEvent > -1)
    title = ((KBKEvent *) [events objectAtIndex:self.deletingEvent]).title;
  else
    title = @"all events";
  
  UIAlertView *delete = [[UIAlertView alloc] initWithTitle:@"Delete Event?"
                                                   message:[NSString stringWithFormat:@"Are you sure you want to delete %@?", title]
                                                  delegate:self
                                         cancelButtonTitle:@"No, cancel"
                                         otherButtonTitles:@"Yes, delete", nil];
  [delete show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
  if ([alertView.title isEqualToString:@"Delete Event?"]) {
    if (buttonIndex == 1) {
      [self deleteEvent];
    }
  }
}

-(void) deleteEvent
{
  NSInteger index;
  if (self.deletingEvent == -1)
    index = 0;
  else
    index = self.deletingEvent;

  while (index < [events count]) {
    KBKEvent *event = ((KBKEvent *) [events objectAtIndex:index]);
    [events removeObjectAtIndex:index];
    
    // remove from starred events
    if (event.starred) {
      [KBKWBCData removeStarredEvent:event];
    }
    
    // remove from day list
    NSMutableArray *events = ((KBKEventGroup *)[[[KBKWBCData dayList] objectAtIndex:event.day] objectAtIndex:event.hour - 6]).events;
    for (int i=0; i<[events count]; i++) {
      if ([((KBKEvent *)[events objectAtIndex:i]).eventID isEqualToString:event.eventID]) {
        [events removeObjectAtIndex:i];
        break;
      }
    }

    if (self.deletingEvent > -1)
      index = [events count];
  }

  // change selected event
  if (self.selectedEvent >= self.deletingEvent)
    self.selectedEvent++;

  if (self.selectedEvent >= [events count])
    self.selectedEvent = [events count]-1;

  [self.tableView reloadData];
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
  if ([segue.identifier isEqualToString:@"UserDataToEvent"])
  {
    // TODO get correct event
    /*
    NSIndexPath *indexPath = [self indexPathForSelectedRow];
    KBKEventViewController *destViewController = segue.destinationViewController;
    destViewController.event = ((KBKEvent *)[events objectAtIndex:indexPath.row]);
     */
  }
}

-(void) viewWillDisappear:(BOOL)animated
{
  // save events
  NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

  KBKEvent *event;
  for (int i=0; i<[events count]; i++) {
    event = events[i];
    [defaults setObject:[NSString stringWithFormat:@"%d~%d~%@~%f~%@", event.day, event.hour, event.title, event.duration, event.location] forKey:[NSString stringWithFormat:@"2014userEvents%d", i]];
  }
  [defaults setObject:@"" forKey:[NSString stringWithFormat:@"2014userEvents%lu", (unsigned long)[events count]]];
  [defaults synchronize];

  [super viewWillDisappear:animated];
}

@end
