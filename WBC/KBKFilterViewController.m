//
//  KBKFilterViewController.m
//  WBC
//
//  Created by Kevin on 6/21/14.
//  Copyright (c) 2014 Boardgame Players Association. All rights reserved.
//

#import "KBKFilterViewController.h"
#import "KBKFilterTableViewCell.h"
#import "KBKWBCData.h"
#import "KBKTournament.h"

@interface KBKFilterViewController ()

@end

@implementation KBKFilterViewController


- (void)viewDidLoad
{
  [super viewDidLoad];
  
  self.tournaments = [KBKWBCData tournaments];
}

-(void) viewWillDisappear:(BOOL)animated
{
  NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
  KBKTournament *tournament;
  NSIndexPath *index;
  BOOL visible;
  for (int i=0; i<[[KBKWBCData tournaments] count]; i++)
  {
    index = [NSIndexPath indexPathForRow:i inSection:0];
    visible = [[self.tableView cellForRowAtIndexPath:index] isSelected];
    
    tournament = ((KBKTournament *)[[KBKWBCData tournaments] objectAtIndex:i]);
    tournament.visible = visible;
    
    NSString *key = [@"vis_" stringByAppendingString:tournament.title];
    [defaults setBool:visible forKey:key];
  }
  
  [defaults synchronize];
  [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
  return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  return [_tournaments count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  KBKTournament *tournament = ((KBKTournament *) [_tournaments objectAtIndex:indexPath.row]);
  
  KBKFilterTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FilterTableCell" forIndexPath:indexPath];
  [cell.itemLabel setText: tournament.title];
  [cell setSelected:tournament.visible];
  
  return cell;
}


/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)addAll:(id)sender {
}

- (IBAction)addTournaments:(id)sender {
}

- (IBAction)subtractAll:(id)sender {
}

- (IBAction)subtractTournaments:(id)sender {
}
@end
