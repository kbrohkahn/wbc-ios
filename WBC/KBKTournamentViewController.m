//
//  KBKTournamentViewController.m
//  WBC
//
//  Created by Kevin on 5/31/14.
//  Copyright (c) 2014 Boardgame Players Association. All rights reserved.
//

#import "KBKTournamentViewController.h"
#import "KBKWBCData.h"
#import "KBKEvent.h"
#import "KBKEventGroup.h"
#import "KBKTournamentTableViewCell.h"
#import "KBKMapContainer.h"
#import "KBKTournament.h"
#import "KBKEventViewController.h"
#import "KBKAddEventController.h"

@implementation KBKTournamentViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}

- (void)viewDidLoad
{
  [super viewDidLoad];
  
  _tournament = ((KBKTournament*) [[KBKWBCData tournaments] objectAtIndex:_tournamentID]);
  
  [self.navigationItem setTitle:self.tournament.title];
  [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"BarButtonIconQuestion"] style:UIBarButtonItemStylePlain target:self action:@selector(openHelp)]];
  
  _events = [[NSMutableArray alloc] init];
  
  NSArray * dayList = [KBKWBCData dayList];
  
  NSMutableArray *searchList;
  for (int i=0; i<[dayList count]; i++) {
    for (int j=1; j<[dayList[i] count]; j++) {
      searchList = ((KBKEventGroup *) dayList[i][j]).events;
      
      for (int k=0; k<[searchList count]; k++) {
        if (((KBKEvent *)searchList[k]).tournamentID == _tournamentID)
          [_events addObject:((KBKEvent *)searchList[k])];
      }
    }
  }
  
  [_tournamentStar setBackgroundImage:[UIImage imageNamed:([self checkAllStar] ? @"StarOn" :@"StarOff")] forState:UIControlStateNormal];

  [_gmLabel setText: [@"GM: " stringByAppendingString:self.tournament.gm]];
  
  if (self.tournament.isTournament) {
    KBKEvent *lastEvent= [_events objectAtIndex: [_events count] - 1];
    
    [_finishSlider setMaximumValue:6];
    
    [_finishLabel setText:[@"Finish: " stringByAppendingString: [NSString stringWithFormat:@"%ld", (long)self.tournament.finish]]];
    [_finishSlider setValue:self.tournament.finish];
    
    BOOL started = lastEvent.day * 24 + lastEvent.hour < [KBKWBCData getDay] * 24 + [KBKWBCData getHour];
    
    [_finishSlider setEnabled:started];
    [_finishLabel setEnabled:started];
  } else {
    //[_reportLink removeFromSuperview];
    //[_previewLink removeFromSuperview];
    
    //[_finishLabel removeFromSuperview];
    //[_finishSlider removeFromSuperview];
  }
}

-(BOOL) checkAllStar
{
  BOOL allStarred = YES;
  
  KBKEvent *event;
  for (int i=0; i<[_events count]; i++)
  {
    event = _events[i];
    if (!event.starred)
    {
      allStarred = NO;
      break;
    }
  }
  return allStarred;
}

- (void)didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
  return 50.0f;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  return [_events count];
}


-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  static NSString *simpleTableIdentifier = @"TournamentPortraitCell";
  KBKTournamentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
  
  if (cell == nil)
    cell = [[KBKTournamentTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
  
  KBKEvent *e =  [_events objectAtIndex:indexPath.row];
  UIColor *textColor = [KBKWBCData getEventTextColor:e];
  UIFont *textFont = [KBKWBCData getEventTextFont:e];
  
  cell.titleLabel.text = e.title;
  [cell.titleLabel setTextColor:textColor];
  [cell.titleLabel setFont:textFont];
  
  cell.locationLabel.color = textColor;
  cell.locationLabel.text = e.location;
  [cell.locationLabel setTextColor:textColor];
  [cell.locationLabel setFont:textFont];
  
  cell.dayLabel.text = [[KBKWBCData displayDayStrings] objectAtIndex:e.day];
  [cell.dayLabel setTextColor:textColor];
  [cell.dayLabel setFont:textFont];
  
  cell.timeLabel.text = [NSString stringWithFormat:@"%li", (long)e.hour];
  [cell.timeLabel setTextColor:textColor];
  [cell.timeLabel setFont:textFont];
  
  cell.durationLabel.text = [NSString stringWithFormat:@"%2.2f", e.duration];
  [cell.durationLabel setTextColor:textColor];
  [cell.durationLabel setFont:textFont];
  
  cell.formatLabel.text = e.eFormat;
  [cell.formatLabel setTextColor:textColor];
  [cell.formatLabel setFont:textFont];
  
  cell.classLabel.text =  e.eClass;
  [cell.classLabel setTextColor:textColor];
  [cell.classLabel setFont:textFont];
  
  cell.starButton.tag =  indexPath.row;
  [cell.starButton setBackgroundImage:[UIImage imageNamed:(e.starred?@"StarOn":@"StarOff")] forState:UIControlStateNormal];
  
  UITapGestureRecognizer *singleFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                    action:@selector(didTapLabelWithGesture:)];
  [cell.locationLabel addGestureRecognizer:singleFingerTap];
  
  
  if ([cell isSelected])
    [cell setBackgroundColor:[[UIColor alloc] initWithRed:.9365f green:.75f blue:.75f alpha:1.0f]];
  else if ([cell isHighlighted])
    [cell setBackgroundColor:[[UIColor alloc] initWithRed:.0f green:.5f blue:1.0f alpha:1.0f]];
  else if (indexPath.row % 2 == 0)
    [cell setBackgroundColor:[KBKWBCData getOddCellColor:e]];
  else
    [cell setBackgroundColor:[KBKWBCData getEvenCellColor:e]];
  
  return cell;
}

- (IBAction)changeEventStar:(id)sender {
  UIButton * starButton = ((UIButton *) sender);
  
  NSInteger tag = starButton.tag;
  
  KBKEvent *e = [_events objectAtIndex:tag];
  e.starred = !e.starred;
  
  [starButton setBackgroundImage:[UIImage imageNamed:(e.starred?@"StarOn":@"StarOff")] forState:UIControlStateNormal];
  
  [KBKWBCData updateStarredEvent:e];
  
  BOOL allStarred = [self checkAllStar];
  [_tournamentStar setBackgroundImage:[UIImage imageNamed:(allStarred ? @"StarOn" :@"StarOff")] forState:UIControlStateNormal];
  
  [self.tableView reloadData];
}

-(void) viewDidAppear:(BOOL)animated
{
  [self.tableView reloadData];
  [_tournamentStar setBackgroundImage:[UIImage imageNamed:([self checkAllStar] ? @"StarOn" :@"StarOff")] forState:UIControlStateNormal];
}

-(IBAction)sliderValueChanged:(id)sender
{
  int intValue = (int) lroundf([_finishSlider value]);
  _finishLabel.text = [@"Finish: " stringByAppendingString:[NSString stringWithFormat:@"%i", intValue]];
  [_finishSlider setValue:intValue];
}

-(IBAction)changeTournamentStar:(id)sender
{
  BOOL allStarred = [self checkAllStar];
  allStarred = !allStarred;
  
  [_tournamentStar setBackgroundImage:[UIImage imageNamed:(allStarred ? @"StarOn" :@"StarOff")] forState:UIControlStateNormal];
  
  KBKEvent *event;
  for (int i=0; i<[_events count]; i++) {
    event = ((KBKEvent *) _events[i]);
    
    if (event.starred != allStarred) {
      event.starred = allStarred;
      
      [KBKWBCData updateStarredEvent:event];
    }
  }
  [_tableView reloadData];
}

-(IBAction)openPreview:(id)sender
{
  NSString *link = [NSString stringWithFormat:@"http://boardgamers.org/yearbkex/%@pge.htm", self.tournament.label];
  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:link]];
}

-(IBAction)openReport:(id)sender
{
  NSString *link = [NSString stringWithFormat:@"http://boardgamers.org/yearbook13/%@pge.htm", self.tournament.label];
  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:link]];
}

- (void)didTapLabelWithGesture:(UITapGestureRecognizer *)tapGesture {
  NSString * room = ((UILabel*)tapGesture.view).text;
  KBKMapContainer *map = [self.storyboard instantiateViewControllerWithIdentifier:@"MapContainer"];
  map.room = room;
  
  [self.navigationController pushViewController:map animated:YES];
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
  if ([segue.identifier isEqualToString:@"TournamentToEvent"])
  {
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    KBKEventViewController *destViewController = segue.destinationViewController;
    destViewController.event = ((KBKEvent *)[_events objectAtIndex:indexPath.row]);
  }
}

-(IBAction)openHelp
{
  UIViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"HelpViewController"];
  [self.navigationController pushViewController:newView animated:YES];
}

-(void) viewWillDisappear:(BOOL)animated
{
  // save finish
  if (self.tournament.isTournament) {
    NSNumber *value = [NSNumber numberWithFloat: [_finishSlider value]];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:value forKey:[@"fin_" stringByAppendingString:self.tournament.title]];
    [defaults synchronize];
  }

  [super viewWillDisappear:animated];
}

@end
