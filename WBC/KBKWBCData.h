//
//  KBKWBCData.h
//  WBC
//
//  Created by Kevin on 6/3/14.
//  Copyright (c) 2014 Boardgame Players Association. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KBKEvent.h"

@interface KBKWBCData : NSObject

+ (NSMutableArray*) dayList;
+ (NSArray*) displayDayStrings;
+ (NSArray*) parsingDayStrings;

+ (NSMutableArray*) tournaments;

+ (UIColor *) getOddCellColor:(KBKEvent*) event;
+ (UIColor *) getEvenCellColor:(KBKEvent*) event;
+ (UIColor *) getEventTextColor:(KBKEvent*) event;
+ (UIFont *) getEventTextFont:(KBKEvent*) event;

+(void) updateStarredEvent:(KBKEvent*)event;
+(void) addStarredEvent:(KBKEvent*)event;
+(void) removeStarredEvent:(KBKEvent*)event;

+(void) loadDayList;
+ (NSInteger) getDay;
+ (NSInteger) getHour;

@end
