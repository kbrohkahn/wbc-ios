//
//  KBKUserDataViewController.h
//  WBC
//
//  Created by Kevin on 5/31/14.
//  Copyright (c) 2014 Boardgame Players Association. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KBKEvent.h"
#import "KBKTournament.h"

@interface KBKUserDataViewController : UIViewController <UIAlertViewDelegate>

@property (strong, nonatomic) IBOutlet UIView *viewContainer;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentedControl;

- (IBAction)controlValueChanged:(id)sender;

@end
