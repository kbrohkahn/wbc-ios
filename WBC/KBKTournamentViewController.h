//
//  KBKTournamentViewController.h
//  WBC
//
//  Created by Kevin on 5/31/14.
//  Copyright (c) 2014 Boardgame Players Association. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KBKEvent.h"
#import "KBKTournament.h"

@interface KBKTournamentViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic) NSInteger tournamentID;
@property (strong, nonatomic) KBKTournament *tournament;
@property (nonatomic) NSInteger selectedEvent;

@property (strong, nonatomic) NSMutableArray* events;

@property (strong, nonatomic) IBOutlet UILabel *finishLabel;
@property (strong, nonatomic) IBOutlet UISlider *finishSlider;

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIButton *previewLink;
@property (strong, nonatomic) IBOutlet UIButton *reportLink;
@property (strong, nonatomic) IBOutlet UILabel *gmLabel;
@property (strong, nonatomic) IBOutlet UIButton *tournamentStar;

-(IBAction)sliderValueChanged:(id)sender;

-(IBAction)openPreview:(id)sender;
-(IBAction)openReport:(id)sender;

-(IBAction)changeTournamentStar:(id)sender;
-(IBAction)changeEventStar:(id)sender;

@end
