//
//  KBKFilterViewController.h
//  WBC
//
//  Created by Kevin on 6/21/14.
//  Copyright (c) 2014 Boardgame Players Association. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KBKFilterViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSMutableArray *tournaments;

@property (strong, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)addAll:(id)sender;
- (IBAction)addTournaments:(id)sender;
- (IBAction)subtractAll:(id)sender;
- (IBAction)subtractTournaments:(id)sender;

@end
