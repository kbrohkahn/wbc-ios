//
//  KBKEvent.m
//  WBC
//
//  Created by Kevin on 5/28/14.
//  Copyright (c) 2014 Boardgame Players Association. All rights reserved.
//

#import "KBKEvent.h"

@implementation KBKEvent

-(id) initWithValues:(NSString *)i tournament:(int)tID date:(int)dat hour:(int)h title:(NSString *)t class:(NSString *)cl format:(NSString *)f qualify:(BOOL)q duration:(double)dur continuous:(BOOL)co totalDuration:(double)td location:(NSString *)l {
  
  self.eventID = i;
  self.tournamentID = tID;
  
  self.day = dat;
  self.hour = h;
  self.title = t;
  
  self.eClass = cl;
  self.eFormat = f;
  self.qualify = q;
  
  self.duration = dur;
  self.continuous = co;
  self.totalDuration = td;
  
  self.location = l;
  
  self.starred = NO;
  
  return self.init;
}
@end
