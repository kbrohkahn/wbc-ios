//
//  KBKAddEventController.h
//  WBC
//
//  Created by Kevin on 7/14/14.
//  Copyright (c) 2014 Boardgame Players Association. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KBKAddEventController : UIViewController <UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UITextField *titleTextField;
@property (strong, nonatomic) IBOutlet UITextField *locationTextField;

@property (strong, nonatomic) IBOutlet UITextField *hourTextField;
@property (strong, nonatomic) IBOutlet UITextField *durationTextField;

@property (strong, nonatomic) NSArray *daySwitches;

@property (strong, nonatomic) IBOutlet UISwitch *switchDay0;
@property (strong, nonatomic) IBOutlet UISwitch *switchDay1;
@property (strong, nonatomic) IBOutlet UISwitch *switchDay2;
@property (strong, nonatomic) IBOutlet UISwitch *switchDay3;
@property (strong, nonatomic) IBOutlet UISwitch *switchDay4;
@property (strong, nonatomic) IBOutlet UISwitch *switchDay5;
@property (strong, nonatomic) IBOutlet UISwitch *switchDay6;
@property (strong, nonatomic) IBOutlet UISwitch *switchDay7;
@property (strong, nonatomic) IBOutlet UISwitch *switchDay8;

@end
