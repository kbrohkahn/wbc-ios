//
//  KBKNoteTableViewCell.h
//  WBC
//
//  Created by Kevin on 2/5/15.
//  Copyright (c) 2015 Boardgame Players Association. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KBKNoteTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UITextView *noteTextView;

@end
