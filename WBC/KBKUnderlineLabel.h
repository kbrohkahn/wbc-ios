//
//  KBKUnderlineLabel.h
//  WBC
//
//  Created by Kevin on 6/4/14.
//  Copyright (c) 2014 Boardgame Players Association. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KBKUnderlineLabel : UILabel

@property (strong, nonatomic) UIColor *color;

@end
