//
//  KBKAboutViewController.h
//  WBC
//
//  Created by Kevin on 7/13/14.
//  Copyright (c) 2014 Boardgame Players Association. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KBKAboutViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *appVersionLabel;

@end
