//
//  KBKViewController.h
//  WBC
//
//  Created by Kevin on 5/28/14.
//  Copyright (c) 2014 Boardgame Players Association. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KBKScheduleTableViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property NSUInteger pageIndex;
@property NSString *titleText;
@property NSArray *eventGroups;

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIView *tableHeader;

- (IBAction)changeEventStar:(id)sender;
@end
