//
//  KBKTournamentTableViewCell.h
//  WBC
//
//  Created by Kevin on 6/3/14.
//  Copyright (c) 2014 Boardgame Players Association. All rights reserved.
//

#import "KBKUnderlineLabel.h"
#import <UIKit/UIKit.h>

@interface KBKTournamentTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet KBKUnderlineLabel *locationLabel;
@property (strong, nonatomic) IBOutlet UIButton *starButton;
@property (strong, nonatomic) IBOutlet UILabel *classLabel;
@property (strong, nonatomic) IBOutlet UILabel *formatLabel;
@property (strong, nonatomic) IBOutlet UILabel *durationLabel;
@property (strong, nonatomic) IBOutlet UILabel *timeLabel;
@property (strong, nonatomic) IBOutlet UILabel *dayLabel;

@end
