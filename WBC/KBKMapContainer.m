//
//  KBKMapContainer.m
//  WBC
//
//  Created by Kevin on 6/10/14.
//  Copyright (c) 2014 Boardgame Players Association. All rights reserved.
//

#import "KBKMapContainer.h"

@interface KBKMapContainer ()

@end

@implementation KBKMapContainer

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}

- (void)viewDidLoad
{
  [super viewDidLoad];
  
  self.navigationItem.title = _room;
  
  [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
  [[NSNotificationCenter defaultCenter] addObserver:self
                                           selector:@selector(setLayout)
                                               name:UIDeviceOrientationDidChangeNotification
                                             object:nil];
}

-(void) viewWillAppear:(BOOL)animated
{
  [self setLayout];
}

- (IBAction)setLayout
{
  NSLog(@"Changing");
  
  [self.mapViewController.view removeFromSuperview];
  [self dismissViewControllerAnimated:NO completion:nil];
  
  UIDeviceOrientation deviceOrientation = [UIDevice currentDevice].orientation;
  
  NSString *identifier;
  if (UIDeviceOrientationIsLandscape(deviceOrientation))
    identifier = @"LandMapViewController";
  else
    identifier = @"PortMapViewController";
  
  // set up map view controller
  self.mapViewController = [self.storyboard instantiateViewControllerWithIdentifier:identifier];
  self.mapViewController.room = _room;
  
  // change the size of page view controller
  self.mapViewController.view.frame = self.view.bounds;
  
  [self addChildViewController:_mapViewController];
  [self.view addSubview:_mapViewController.view];
  
  [self.mapViewController didMoveToParentViewController:self];
}

- (void)didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

@end
