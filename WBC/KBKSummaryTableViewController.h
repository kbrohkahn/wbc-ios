//
//  KBKSummaryTableViewController.h
//  WBC
//
//  Created by Kevin on 8/2/14.
//  Copyright (c) 2014 Boardgame Players Association. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KBKSummaryTableViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, UIActionSheetDelegate>

@property (strong, nonatomic) NSMutableArray *starredEvents;

@property (strong, nonatomic) IBOutlet UITableView *tableView;

- (IBAction)changeEventStar:(id)sender;

@end
