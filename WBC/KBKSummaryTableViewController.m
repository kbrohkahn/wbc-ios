//
//  KBKSummaryTableViewController.m
//  WBC
//
//  Created by Kevin on 8/2/14.
//  Copyright (c) 2014 Boardgame Players Association. All rights reserved.
//

#import "KBKSummaryTableViewController.h"
#import "KBKSummaryTableViewCell.h"
#import "KBKScheduleViewController.h"
#import "KBKTournamentViewController.h"
#import "KBKTournament.h"
#import "KBKWBCData.h"
#import "KBKEvent.h"
#import "KBKEventGroup.h"
#import "KBKMapContainer.h"
#import "KBKSettingsViewController.h"
#import "KBKFilterViewController.h"
#import "KBKUserDataViewController.h"


@interface KBKSummaryTableViewController ()

@end

@implementation KBKSummaryTableViewController

- (void)viewDidLoad
{
  
  // THIS IS A TEST
  [super viewDidLoad];

  [KBKWBCData loadDayList];

  // set up navigation bar buttons
  NSMutableArray *rightButtons = [[NSMutableArray alloc]init];
  [rightButtons addObject:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(openSearch)]];
  [rightButtons addObject:[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"BarButtonIconQuestion"] style:UIBarButtonItemStylePlain target:self action:@selector(openHelp)]];
  [self.navigationItem setRightBarButtonItems:rightButtons];

  NSMutableArray *leftButtons = [[NSMutableArray alloc]init];
  [leftButtons addObject:[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"BarButtonIconEllipsis"] style:UIBarButtonItemStylePlain target:self action:@selector(openActionSheet)]];
  [self.navigationItem setLeftBarButtonItems:leftButtons];


  // TODO notify of changes
  /*
  NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
  NSInteger version = [defaults integerForKey:@"appVersion"];

  [defaults setInteger:2 forKey:@"appVersion"];
  [defaults synchronize];
  */

  // instantiate summary data
  NSMutableArray *dayList = [KBKWBCData dayList];
  self.starredEvents = [[NSMutableArray alloc] init];
  
  for (int i=0; i< [dayList count]; i++) {
    [self.starredEvents addObject:[[NSMutableArray alloc] init]];
  }
  
  BOOL noneStarred = true;
  NSMutableArray *events;
  KBKEvent *event;
  for (int i=0; i<[dayList count]; i++) {
    events = ((KBKEventGroup *)[[dayList objectAtIndex:i] objectAtIndex:0]).events;
    
    for (int j=0; j<[events count]; j++) {
      event = [events objectAtIndex:j];
      
      //KBKEvent *summaryEvent = [[KBKEvent alloc] initWithValues:event.eventID tournament:event.tournamentID date:event.day hour:event.hour title:event.title class:event.eClass format:event.eFormat qualify:event.qualify duration:event.totalDuration continuous:event.continuous totalDuration:event.totalDuration location:event.location];
      [[self.starredEvents objectAtIndex:i] addObject:event];
      noneStarred = false;
    }
  }

  [self.tableView reloadData];

  if (noneStarred) {
    UIAlertView *starredAlert = [[UIAlertView alloc] initWithTitle:@"No starred events?"
                                                         message:@"Star events from the Full Schedule to add them to your summary"
                                                        delegate:nil
                                               cancelButtonTitle:@"Go to Full Schedule"
                                               otherButtonTitles:nil];
    [starredAlert show];
  }
  
  NSInteger day = [KBKWBCData getDay];
  if (day > -1) {
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:day] atScrollPosition:UITableViewScrollPositionTop animated:YES];
  }
}

-(void) viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];
  
  // instantiate summary data
  NSMutableArray *dayList = [KBKWBCData dayList];
  self.starredEvents = [[NSMutableArray alloc] init];
  
  for (int i=0; i< [dayList count]; i++) {
    [self.starredEvents addObject:[[NSMutableArray alloc] init]];
  }
  
  BOOL noneStarred = true;
  NSMutableArray *events;
  KBKEvent *event;
  for (int i=0; i<[dayList count]; i++) {
    events = ((KBKEventGroup *)[[dayList objectAtIndex:i] objectAtIndex:0]).events;
    
    for (int j=0; j<[events count]; j++) {
      event = [events objectAtIndex:j];
      
      //KBKEvent *summaryEvent = [[KBKEvent alloc] initWithValues:event.eventID tournament:event.tournamentID date:event.day hour:event.hour title:event.title class:event.eClass format:event.eFormat qualify:event.qualify duration:event.totalDuration continuous:event.continuous totalDuration:event.totalDuration location:event.location];
      [[self.starredEvents objectAtIndex:i] addObject:event];
      noneStarred = false;
    }
  }

  [self.tableView reloadData];

  if (noneStarred) {
    UIAlertView *starredAlert = [[UIAlertView alloc] initWithTitle:@"No starred events"
                                                         message:@"Starredar events from the Full Schedule to add them to your summary"
                                                        delegate:self
                                               cancelButtonTitle:@"Go to Full Schedule"
                                               otherButtonTitles:nil];
    [starredAlert show];
  }
  
  NSInteger day = [KBKWBCData getDay];
  if (day > -1) {
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:day] atScrollPosition:UITableViewScrollPositionTop animated:YES];
  }
}

-(void) viewDidAppear:(BOOL)animated
{
  [super viewDidAppear:animated];

  self.navigationController.navigationBar.topItem.title = @"All Starred Events";
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
  if ([alertView.title isEqualToString:@"No starred events"]) {
    UIViewController *newView = (KBKScheduleViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"ScheduleViewController"];
    [self.navigationController pushViewController:newView animated:YES];
  }
}

- (void)didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
  return [self.starredEvents count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  return [((NSMutableArray *) [self.starredEvents objectAtIndex:section]) count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  static NSString *simpleTableIdentifier = @"SummaryTableViewCell";
  KBKSummaryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
  
  if (cell == nil)
    cell = [[KBKSummaryTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
  
  KBKEvent *event = [((NSMutableArray *)[self.starredEvents objectAtIndex:indexPath.section]) objectAtIndex:indexPath.row];
  
  UIColor *textColor = [KBKWBCData getEventTextColor:event];
  UIFont *textFont = [KBKWBCData getEventTextFont:event];
  
  // title
  cell.titleLabel.text = event.title;
  [cell.titleLabel setTextColor:textColor];
  [cell.titleLabel setFont:textFont];
  
  // duration
  if (event.continuous)
    cell.durationLabel.text = [NSString stringWithFormat:@"%2.2f C", event.duration];
  else
    cell.durationLabel.text = [NSString stringWithFormat:@"%2.2f", event.duration];
  [cell.durationLabel setTextColor:textColor];
  [cell.durationLabel setFont:textFont];
  
  // notes
  cell.noteTextView.text = event.note;
  [cell.noteTextView setTextColor:textColor];
  [cell.noteTextView setFont:textFont];
  
  // location
  cell.locationLabel.color = textColor;
  cell.locationLabel.text = event.location;
  [cell.locationLabel setTextColor:textColor];
  [cell.locationLabel setFont:textFont];
  
  UITapGestureRecognizer *singleFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapLabelWithGesture:)];
  [cell.locationLabel addGestureRecognizer:singleFingerTap];
  
  // star
  cell.starButton.tag = indexPath.section * 1000 + indexPath.row;
  [cell.starButton setBackgroundImage:[UIImage imageNamed:(event.starred?@"StarOn":@"StarOff")] forState:UIControlStateNormal];
  
  // background
  if ([cell isSelected]) {
    [cell setBackgroundColor:[[UIColor alloc] initWithRed:.9365f green:.75f blue:.75f alpha:1.0f]];
    [cell.noteTextView setBackgroundColor:[[UIColor alloc] initWithRed:.9365f green:.75f blue:.75f alpha:1.0f]];
  }
  else if ([cell isHighlighted]) {
    [cell setBackgroundColor:[[UIColor alloc] initWithRed:.0f green:.5f blue:1.0f alpha:1.0f]];
    [cell.noteTextView setBackgroundColor:[[UIColor alloc] initWithRed:.0f green:.5f blue:1.0f alpha:1.0f]];
  }
  else if (indexPath.row % 2 == 0) {
    [cell setBackgroundColor:[KBKWBCData getOddCellColor:event]];
    [cell.noteTextView setBackgroundColor:[KBKWBCData getOddCellColor:event]];
  }
  else {
    [cell setBackgroundColor:[KBKWBCData getEvenCellColor:event]];
    [cell.noteTextView setBackgroundColor:[KBKWBCData getEvenCellColor:event]];
  }
  
  return cell;
}

- (void)didTapLabelWithGesture:(UITapGestureRecognizer *)tapGesture {
  NSString * room = ((UILabel*)tapGesture.view).text;
  KBKMapContainer *map = [self.storyboard instantiateViewControllerWithIdentifier:@"MapContainer"];
  map.room = room;
  
  [self.navigationController pushViewController:map animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
  KBKEvent *event = [((NSMutableArray *)[self.starredEvents objectAtIndex:indexPath.section]) objectAtIndex:indexPath.row];
  
  if ([event.note length] == 0) {
    return 60;
  } else {
    return 128;
  }
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
  static NSString *simpleTableIdentifier = @"SummaryTableGroup";
  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
  
  if (cell == nil)
  {
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
  }
  
  NSString * title = [[KBKWBCData displayDayStrings] objectAtIndex:section];
  [cell.textLabel setText:title];
  return cell;

}

-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
  return 48;
}

-(BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
  return YES;
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
  if ([segue.identifier isEqualToString:@"SummaryToTournament"])
  {
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    KBKEvent *e =((KBKEvent *)[((NSMutableArray *) [_starredEvents objectAtIndex:indexPath.section]) objectAtIndex:indexPath.row]);
    
    KBKTournamentViewController *destViewController = segue.destinationViewController;
    destViewController.tournamentID = e.tournamentID;
  }
}

- (IBAction)changeEventStar:(id)sender {
  NSInteger tag = ((UIButton *) sender).tag;
  
  KBKEvent *e = [((NSMutableArray *) [_starredEvents objectAtIndex:tag/1000]) objectAtIndex:tag%1000];
  e.starred = !e.starred;
  [KBKWBCData updateStarredEvent:e];
  [self.tableView reloadData];
}

-(IBAction)openSearch
{
  UIViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchTableViewController"];
  [self.navigationController pushViewController:newView animated:YES];
}

-(IBAction)openHelp
{
  UIViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"HelpViewController"];
  [self.navigationController pushViewController:newView animated:YES];
}

-(IBAction)openActionSheet
{
  UIActionSheet *menu = [[UIActionSheet alloc] initWithTitle:@"WBC Schedule Menu" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Full Schedule", @"My WBC Data", @"Filter", @"Settings", @"About", nil];
  
  [menu showInView:self.view];
}

-(void) actionSheet:(UIActionSheet*)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
  UIViewController *newView = nil;
  if (buttonIndex == 0) {
    // SCHEDULE
    newView = (KBKScheduleViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"ScheduleViewController"];
  } else if (buttonIndex == 1) {
    // USER DATA
    KBKUserDataViewController *userDataView = (KBKUserDataViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"UserDataViewController"];
    [self.navigationController pushViewController:userDataView animated:YES];
  } else if (buttonIndex == 2) {
    // FILTER
    newView = (KBKFilterViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"FilterViewController"];
  } else if (buttonIndex == 3) {
    // SETTINGS
    newView = (KBKSettingsViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"SettingsViewController"];
  } else if (buttonIndex == 4) {
    // ABOUT
    newView = [self.storyboard instantiateViewControllerWithIdentifier:@"AboutViewController"];
  }

  if (newView == nil) {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                    message:@"Item not available."
                                                   delegate:nil
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles:nil];
    [alert show];
  } else {
    [self.navigationController pushViewController:newView animated:YES];
  }
}


-(void) viewWillDisappear:(BOOL)animated {
  UIApplication* app = [UIApplication sharedApplication];
  NSArray* oldNotifications = [app scheduledLocalNotifications];
  
  if ([oldNotifications count] > 0)
    [app cancelAllLocalNotifications];
  
  NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
  if ([defaults boolForKey:@"notifyEvents"]) {
    NSInteger minutesPrior = [defaults integerForKey:@"notifyTime"];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    [calendar setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en-US"]];
    // force US locale, because other countries (e.g. the rest of the world) might use different weekday numbering
    
    NSDateComponents *dateComponents = [calendar components:NSYearCalendarUnit | NSWeekCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit fromDate:[NSDate date]];
    
    [dateComponents setMonth:8];
    [dateComponents setMinute:60 - minutesPrior];
    [dateComponents setSecond:0];
    
    NSInteger day = [KBKWBCData getDay];
    if (day == -1) {
      day = 0;
    }
    
    for (; day<[[KBKWBCData dayList] count]; day++) {
      NSMutableArray *events = ((KBKEventGroup *) [[[KBKWBCData dayList] objectAtIndex:day] objectAtIndex:0]).events;
      
      for (int j=0; j<[events count]; j++) {
        UILocalNotification* alarm = [[UILocalNotification alloc] init];
        if (alarm)
        {
          KBKEvent *event = ((KBKEvent *)[events objectAtIndex:j]);
          
          if (event.day * 24 + event.hour <= [KBKWBCData getDay] * 24 + [KBKWBCData getHour])
            continue;
          
          [dateComponents setDay:2 + event.day];
          [dateComponents setHour:event.hour - 1];
          
          NSDate *date = [calendar dateFromComponents:dateComponents];
          
          alarm.fireDate =  date;
          alarm.soundName = UILocalNotificationDefaultSoundName;
          alarm.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"EDT"];
          alarm.alertBody = [@"WBC event starting soon: " stringByAppendingString:event.title];
          
          [app scheduleLocalNotification:alarm];
        }
      }
    }
  }
}

@end
