//
//  KBKUserEventsViewController.h
//  WBC
//
//  Created by Kevin on 8/2/14.
//  Copyright (c) 2014 Boardgame Players Association. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KBKUserEventsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic) NSInteger selectedEvent;
@property (nonatomic) NSInteger deletingEvent;

+ (NSMutableArray*) events;

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIButton *tournamentStar;

- (IBAction)changeTournamentStar:(id)sender;
- (IBAction)changeEventStar:(id)sender;
- (IBAction)deleteAll:(id)sender;
- (IBAction)addEvent:(id)sender;

@end
