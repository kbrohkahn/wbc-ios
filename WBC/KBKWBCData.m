//
//  KBKWBCData.m
//  WBC
//
//  Created by Kevin on 6/3/14.
//  Copyright (c) 2014 Boardgame Players Association. All rights reserved.
//

#import "KBKWBCData.h"
#import "KBKEvent.h"
#import "KBKEventGroup.h"
#import "KBKTournament.h"

@implementation KBKWBCData

static NSMutableArray* tournaments;
+ (NSMutableArray*) tournaments
{ @synchronized(self) { return tournaments; } }

static NSMutableArray* dayList;
+ (NSMutableArray*) dayList
{ @synchronized(self) { return dayList; } }

static NSArray* displayDayStrings;
+ (NSArray*) displayDayStrings
{ @synchronized(self) { return displayDayStrings; } }

static NSArray* parsingDayStrings;
+ (NSArray*) parsingDayStrings
{ @synchronized(self) { return parsingDayStrings; } }

+(NSInteger) getDay
{
  NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
  [dateFormatter setDateFormat:@"yyyy-MM-dd"];
  NSString *dateString = [dateFormatter stringFromDate:[NSDate date]];
  
  // parse day and time
  NSInteger day = -1;
  for (int i=0; i<[parsingDayStrings count]; i++) {
    if ([dateString isEqualToString:parsingDayStrings[i]])
    {
      day = i;
      break;
    }
  }
  return day;
}

+(NSInteger) getHour
{
  NSDateComponents *components = [[NSCalendar currentCalendar] components:NSHourCalendarUnit fromDate:[NSDate date]];
  return [components hour];
}

+(void) loadDayList
{
  displayDayStrings = @[@"PC Sa 8/2", @"PC Su 8/3", @"Mo 8/4", @"Tu 8/5", @"We 8/6", @"Th 8/7",@"Fr 8/8",@"Sa 8/9",@"Su 8/10",];
  parsingDayStrings = @[@"2014-08-02",@"2014-08-03",@"2014-08-04",@"2014-08-05",@"2014-08-06",@"2014-08-07",@"2014-08-08",@"2014-08-09",@"2014-08-10"];
  
  NSArray *hourStrings = @[@"My events", @"0700", @"0800", @"0900", @"1000", @"1100", @"1200", @"1300", @"1400", @"1500", @"1600", @"1700", @"1800", @"1900", @"2000", @"2100", @"2200", @"2300", @"2400"];
  
  NSArray *preExtraStrings = @[@" AFC", @" NFC", @" FF", @" PC", @" Circus", @" After Action", @" Aftermath"];
  NSArray *postExtraStrings = @[@" Demo"];
  
  NSArray *nonTournamentStrings = @[@"Registration", @"Texas Roadhouse BPA Fundraiser",       @"Vendors Area", @"Wits & Wagers",  @"World at War"];
  
  // initiate day list arrays
  dayList = [[NSMutableArray alloc] initWithCapacity:[parsingDayStrings count]];
  for (int i=0 ; i<[parsingDayStrings count]; i++) {
    [dayList addObject:[[NSMutableArray alloc] initWithCapacity:[hourStrings count]]];
    
    for (int j=0; j<[hourStrings count]; j++) {
      [[dayList objectAtIndex:i] addObject:[[KBKEventGroup alloc] initWithValues:[hourStrings objectAtIndex:j] events:[[NSMutableArray alloc] init]] ];
      
    }
  }
  
  NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
  NSInteger scheduleVersion = [defaults integerForKey:@"scheduleVersion"];
  NSInteger newVersion = scheduleVersion;
  
  NSLog(@"Starting parsing");
  
  /***** PARSE SCHEDULE *****/
  
  int numTournaments=0;
  int numPreviews=0;
  int numJuniors=0;
  int numSeminars=0;
  
  NSString *tournamentTitle = nil;
  NSString *tournamentLabel = nil;
  NSString *shortEventTitle = nil;
  
  NSString *eventTitle, *eClass, *format, *gm, *dayString,  *location, *temp;
  
  int day, hour, prize;
  double duration, totalDuration;
  BOOL visible, continuous, qualify, isTournamentEvent;
  NSInteger tournamentID=-1;
  int index;
  NSRange searchIndex;
  
  NSString *change, *allChanges=@"";
  KBKEvent *event, *tempEvent, *prevEvent;
  
  // load tournament file
  NSString *fullPath = [[NSBundle mainBundle] pathForResource:@"schedule2014" ofType:@"txt"];
  NSError *error;
  NSString *csvData = [NSString stringWithContentsOfFile:fullPath encoding:NSUTF8StringEncoding error:&error];
  NSArray *rawData = [csvData componentsSeparatedByString:@"\n"];
  NSArray *singleItem = [NSArray array];
  NSString *eventString;
  
  tournaments = [[NSMutableArray alloc] init];
  KBKTournament *tournament = [[KBKTournament alloc] initWithValues:0 title:@"My Events" label:@"" isTournament:NO prize:0 gm:@"Me"];
  
  temp =[@"vis_" stringByAppendingString:@"My Events"];
  if (scheduleVersion == 0) {
    [defaults setBool:YES forKey:temp];
    visible = YES;
  } else {
    visible =[defaults boolForKey:temp];
  }
  tournament.visible = visible;
  
  [tournaments addObject:tournament];
  
  // load user events
  index = 0;
  for (;; index++) {
    eventString = [defaults objectForKey:[NSString stringWithFormat:@"2014userEvents%d", index]];
    
    if ([eventString isEqualToString:@""] || eventString == nil){
      break;
    }
    
    singleItem = [eventString componentsSeparatedByString:@"~"];
    day = [singleItem[0] intValue];
    hour = [singleItem[1] intValue];
    eventTitle = singleItem[2];
    duration = [singleItem[3] doubleValue];
    location = singleItem[4];
    
    temp = [[NSString stringWithFormat:@"%d", day * 24 + hour] stringByAppendingString:eventTitle];
    
    event = [[KBKEvent alloc] initWithValues:temp tournament:0 date:day hour:hour title:eventTitle class:@"" format:@"" qualify:NO duration:duration continuous:NO totalDuration:duration location:location];
    event.starred=[defaults boolForKey:[@"starredEvent2014" stringByAppendingString:temp]];
    event.note = [defaults stringForKey:[@"note_" stringByAppendingString:event.eventID]];
    
    [((KBKEventGroup *)dayList[event.day] [event.hour-6]).events addObject:event];
    if (event.starred) {
      [self addStarredEvent:event];
    }
  }
  
  index = -1;
  
  NSMutableArray* searchList;
  
  // set default prefs if not yet opened (need number of events)
  
  NSLog(@"%d", (int) scheduleVersion);
  if (scheduleVersion == 0)
  {
    NSLog(@"Not opened");
  }
  
  int lineNum = 0;
  for (; lineNum < [rawData count]; lineNum++)
  {
    eventString = [NSString stringWithFormat:@"%@", rawData[lineNum]];
    singleItem = [eventString componentsSeparatedByString:@"~"];
    
    // day
    dayString=singleItem[0];
    for (index=0; index<[parsingDayStrings count]; index++) {
      if ([parsingDayStrings[index] isEqualToString:dayString])
        break;
    }
    if (index==[parsingDayStrings count]) {
      NSLog(@"Unknown date:%@ in %@", singleItem[0],singleItem[2]);
      index=0;
    }
    day=index;
    
    // time
    hour=[singleItem[1] intValue];
    
    // title
    eventTitle=singleItem[2];
    
    if ([singleItem count] < 8) {
      prize = 0;
      eClass = @"";
      format = @"";
      duration = 0;
      continuous = NO;
      gm = @"";
      location = @"";
    } else {
      
      // prize
      temp=singleItem[3];
      if ([temp isEqualToString:@""]||[temp isEqualToString:@"-"])
        temp=@"0";
      
      prize = [temp doubleValue];
      
      // class
      eClass=singleItem[4];
      
      // format
      format=singleItem[5];
      
      // duration
      temp=singleItem[6];
      if ([temp isEqualToString:@""]||[temp isEqualToString:@"-"])
        duration=0;
      else
        duration=[temp doubleValue];
      
      if (duration>.33&&duration<.34)
        duration=.33;
      
      // continuous
      continuous=[singleItem[7] isEqualToString:@"Y"];
      
      // gm
      gm=singleItem[8];
      
      // location
      location=singleItem[9];
    }
    
    // get tournament title and label and event short name
    tournamentTitle=eventTitle;
    
    // search through extra strings
    for (int i=0; i<[preExtraStrings count]; i++) {
      tournamentTitle = [tournamentTitle stringByReplacingOccurrencesOfString:preExtraStrings[i] withString:@""];
    }
    
    // split title in two, first part is tournament title,
    // second is short event title (H1/1)
    isTournamentEvent=[eClass length] > 0;
    
    if (isTournamentEvent || [format isEqualToString:@"Preview"] ) {
      searchIndex = [tournamentTitle rangeOfString:@" " options:NSBackwardsSearch];
      shortEventTitle=[tournamentTitle substringFromIndex:searchIndex.location + 1];
      tournamentTitle = [tournamentTitle substringToIndex:searchIndex.location];
    }
    
    if ([tournamentTitle rangeOfString:@"Junior"].length > 0 || [format isEqualToString:@"Preview"] || [format isEqualToString:@"SOG"] || [tournamentTitle hasPrefix:@"COIN"]) {
      tournamentLabel=@"";
      
    } else if (isTournamentEvent) {
      // search through extra strings
      for (int i=0; i<[postExtraStrings count]; i++) {
        tournamentTitle = [tournamentTitle stringByReplacingOccurrencesOfString:postExtraStrings[i] withString:@""];
      }
      
      tournamentLabel = [singleItem[10] substringToIndex:3];
    } else {
      tournamentLabel=@"";
      
      if ([tournamentTitle rangeOfString:@"Auction"].location == 0)
        tournamentTitle = @"Auction Store";
      
      for (int i=0; i<[nonTournamentStrings count]; i++) {
        searchIndex = [eventTitle rangeOfString:nonTournamentStrings[i]];
        if (searchIndex.location==0) {
          tournamentTitle=nonTournamentStrings[i];
          break;
        }
      }
    }
    
    // ADD TOURNAMENT
    tournamentID= -1;
    for (int i=0; i<[tournaments count]; i++) {
      if ([((KBKTournament*) [tournaments objectAtIndex:i]).title isEqualToString:tournamentTitle])
      {
        tournamentID = i;
        break;
      }
    }
    if (tournamentID == -1) {
      tournamentID =[tournaments count];
      tournament = [[KBKTournament alloc] initWithValues:tournamentID title:tournamentTitle label:tournamentLabel isTournament:(isTournamentEvent && [eventTitle rangeOfString:@"Junior"].location == NSNotFound) prize:prize gm:gm];
      
      if ([format isEqualToString:@"Preview"])
        numPreviews++;
      else if ([eventTitle rangeOfString:@"Junior"].location != NSNotFound)
        numJuniors++;
      else if ([format isEqualToString:@"Seminar"])
        numSeminars++;
      else if (isTournamentEvent||[format isEqualToString:@"Demo"])
        numTournaments++;
      
      temp =[@"vis_" stringByAppendingString:tournamentTitle];
      if (scheduleVersion == 0) {
        [defaults setBool:YES forKey:temp];
        visible = YES;
      } else {
        visible =[defaults boolForKey:temp];
      }
      
      tournament.visible = visible;
      tournament.finish = [defaults integerForKey:[@"fin_" stringByAppendingString:tournamentTitle]];
      
      [tournaments addObject:tournament];
      
    } else {
      tournament = ((KBKTournament *) tournaments[tournamentID]);
      
      if ((isTournamentEvent && [eventTitle rangeOfString:@"Junior"].location == NSNotFound))
        tournament.isTournament = YES;
      if (prize > 0)
        tournament.prize = prize;
    }
    
    //NSLog(@"T: %@;;;E: %@", tournamentTitle, eventTitle);
    
    totalDuration=duration;
    qualify=false;
    
    if (isTournamentEvent) {
      if ([shortEventTitle hasPrefix:@"SF"]) {
        qualify=true;
      } else if ([shortEventTitle hasPrefix:@"QF"]) {
        qualify=true;
      } else if ([shortEventTitle hasPrefix:@"F"]) {
        qualify=true;
      } else if ([shortEventTitle hasPrefix:@"QF/SF/F"]) {
        qualify=true;
        totalDuration*=3;
      } else if ([shortEventTitle hasPrefix:@"SF/F"]) {
        qualify=true;
        totalDuration*=2;
      } else if (continuous&&[shortEventTitle hasPrefix:@"R"]
                 &&[shortEventTitle rangeOfString:@"/"].location != NSNotFound) {
        NSInteger dividerIndex=[shortEventTitle  rangeOfString:@"/"].location;
        
        temp = [shortEventTitle substringToIndex:dividerIndex];
        temp = [temp substringFromIndex:1];
        int startRound=[ temp intValue];
        
        int endRound=[[shortEventTitle substringFromIndex:dividerIndex+1] intValue];
        
        int currentTime=hour;
        for (int round=0; round<endRound-startRound; round++) {
          // if time passes midnight, next round
          // starts at
          // 9 the next day
          if (currentTime>24) {
            if (currentTime>=24+9)
              NSLog(@"Event %@ goes past 9", eventTitle);
            totalDuration+=9-(currentTime-24);
            currentTime=9;
          }
          
          totalDuration+=duration;
          currentTime+=duration;
          
        }
        
        if (prevEvent.tournamentID==tournamentID) {
          // update previous event total duration
          temp=prevEvent.title;
          
          // search through extra strings
          for (int i=0; i<[preExtraStrings count]; i++) {
            temp = [temp stringByReplacingOccurrencesOfString:preExtraStrings[i] withString:@""];
            
          }
          
          searchIndex=[temp rangeOfString:@" " options:NSBackwardsSearch];
          
          if (searchIndex.length > 0) {
            temp=[temp substringFromIndex: searchIndex.location+1];
            if ([temp hasPrefix: @"R"] && [temp  rangeOfString:@"/"].location != NSNotFound){
              
              dividerIndex=[temp  rangeOfString:@"/"].location;
              
              temp = [temp substringToIndex:dividerIndex];
              temp = [temp substringFromIndex:1];
              int prevStartRound=[ temp intValue];
              
              
              int realNumRounds=startRound-prevStartRound;
              
              currentTime=hour;
              prevEvent.totalDuration=0;
              for (int round=0; round<realNumRounds; round++) {
                // if time passes midnight, next round
                // starts at
                // 9 the next day
                if (currentTime>24) {
                  if (currentTime>=24+9)
                    
                    NSLog(@"Event %@ goes past 9", prevEvent.title);
                  
                  prevEvent.totalDuration+=9-(currentTime-24);
                  currentTime=9;
                }
                
                prevEvent.totalDuration+=prevEvent.duration;
                currentTime+=prevEvent.duration;
                
              }
              
              searchList=((KBKEventGroup *)dayList[prevEvent.day] [prevEvent.hour-6]).events;
              for (int i=0; i<[searchList count]; i++) {
                if (((KBKEvent *)searchList[i]).eventID==prevEvent.eventID)
                  ((KBKEvent *)searchList[i]).totalDuration=prevEvent.totalDuration;
              }
              
              searchList=((KBKEventGroup *)dayList[prevEvent.day] [0]).events;
              for (int i=0; i<[searchList count]; i++) {
                if (((KBKEvent *)searchList[i]).eventID==prevEvent.eventID)
                  ((KBKEvent *)searchList[i]).totalDuration=prevEvent.totalDuration;
              }
              
              NSLog(@"Event %@ duration changed to %f", prevEvent.title, prevEvent.totalDuration);
              
            }
          }
        }
        
      } else if (continuous) {
        NSLog(@"Unknown continuous event: %@", eventTitle);
      }
    } else if (continuous) {
      NSLog(@"Non tournament event %@ is cont", eventTitle);
    }
    
    temp = [[NSString stringWithFormat:@"%d", day * 24 + hour] stringByAppendingString:eventTitle];
    
    event = [[KBKEvent alloc] initWithValues:temp tournament: (int) tournamentID date:day hour:hour title:eventTitle class:eClass format:format qualify:qualify duration:duration continuous:continuous totalDuration:totalDuration location:location];
    event.starred=[defaults boolForKey:[@"starredEvent2014" stringByAppendingString:temp]];
    event.note = [defaults stringForKey:[@"note_" stringByAppendingString:event.eventID]];
    
    prevEvent=event;
    
    /********* LOAD INTO DAYLIST *************/
    change=@"";
    /*
     if ([event.title isEqualToString:@"Age of Renaissance H1/3 PC"]) {
     int newDay=5;
     
     if (scheduleVersion<0) {
     change = eventTitle;
     change = [change stringByAppendingString:@": Day changed from "];
     change = [change stringByAppendingString:parsingDayStrings[event.day]];
     change = [change stringByAppendingString:@" to "];
     change = [change stringByAppendingString:parsingDayStrings[newDay]];
     
     newVersion = 2;
     }
     event.day=newDay;
     }
     */
    newVersion = 1;
    
    if (![change isEqualToString:@""]) {
      allChanges = [allChanges stringByAppendingString:@"\t"];
      allChanges = [allChanges stringByAppendingString:change];
      allChanges = [allChanges stringByAppendingString:@"\n\n"];
    }
    
    // LOAD INTO LIST
    searchList=((KBKEventGroup *)dayList[event.day] [event.hour-6]).events;
    if ([eventTitle rangeOfString:@"Junior"].location != NSNotFound) {
      index = 0;
      for (; index<[searchList count]; index++) {
        tempEvent=((KBKEvent *)searchList[index]);
        if ([tempEvent.title rangeOfString:@"Junior"].location == NSNotFound && tempEvent.tournamentID > 0)
          break;
      }
    }  else if (!isTournamentEvent || [format isEqualToString:@"Demo"]) {
      index=0;
      for (; index<[searchList count]; index++) {
        tempEvent=((KBKEvent *)searchList[index]);
        if ([tempEvent.eClass length] > 0 && [tempEvent.title rangeOfString:@"Junior"].location == NSNotFound && ![tempEvent.eFormat isEqualToString:@"Demo"])
          break;
      }
    } else if (event.qualify) {
      index=(int) [searchList count];
    } else {
      index=0;
      for (; index<[searchList count]; index++) {
        tempEvent=searchList[index];
        if (tempEvent.qualify)
          break;
      }
    }
    
    [((KBKEventGroup *)dayList[event.day] [event.hour-6]).events insertObject:event atIndex:index];
    if (event.starred) {
      [self addStarredEvent:event];
    }
  }
  
  // save update version
  [defaults setInteger:newVersion forKey:@"scheduleVersion"];
  [defaults synchronize];
  
  NSLog(@"Finished load, %lu total tournaments and %d total events", (unsigned long)tournamentID , lineNum);
  NSLog(@"Of total, %d are tournaments, %d are juniors, %d are previews, %d are seminars", numTournaments, numJuniors, numPreviews, numSeminars);
}

+(void) updateStarredEvent:(KBKEvent *)event
{
  if (event.starred) {
    [self addStarredEvent:event];
  }
  else {
    [self removeStarredEvent:event];
  }
  
  // save star
  NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
  [defaults setBool:event.starred forKey:[@"starredEvent2014" stringByAppendingString:event.eventID]];
  [defaults synchronize];
}

+(void) addStarredEvent:(KBKEvent *)event
{
  KBKEvent *temp;
  int index = 0;
  
  NSMutableArray *events = ((KBKEventGroup *) [[[KBKWBCData dayList] objectAtIndex:event.day] objectAtIndex:0]).events;
  for (; index < [events count]; index++) {
    temp = ((KBKEvent *) events[index]);
    if (temp.hour > event.hour || (temp.hour == event.hour && [temp.title compare:event.title] == NSOrderedAscending))
      break;
  }
  
  [events insertObject:event atIndex:index];
}

+(void) removeStarredEvent:(KBKEvent *)event
{
  NSMutableArray *events = ((KBKEventGroup *) [[[KBKWBCData dayList] objectAtIndex:event.day] objectAtIndex:0]).events;
  [events removeObject:event];
}

+(UIColor *) getOddCellColor:(KBKEvent *) event  {
  BOOL finished = event.day * 24 + event.hour +event.totalDuration < [KBKWBCData getDay] * 24 + [KBKWBCData getHour] + .01;
  BOOL started = event.day * 24 + event.hour <= [KBKWBCData getDay] * 24 + [KBKWBCData getHour];
  
  if (finished)
    return [[UIColor alloc] initWithRed:.8f green:.8f blue:.8f alpha:1.0f];
  else if (started)
    return [[UIColor alloc] initWithRed:.898f green:1.0f blue:.898f alpha:1.0f];
  else
    return [[UIColor alloc] initWithRed:1.0f green:1.0f blue:.898f alpha:1.0f];
}

+(UIColor *) getEvenCellColor:(KBKEvent *) event  {
  BOOL finished = event.day * 24 + event.hour +event.totalDuration < [KBKWBCData getDay] * 24 + [KBKWBCData getHour] + .01;
  BOOL started = event.day * 24 + event.hour <= [KBKWBCData getDay] * 24 + [KBKWBCData getHour];
  
  if (finished)
    return [[UIColor alloc] initWithRed:.6f green:.6f blue:.6f alpha:1.0f];
  else if (started)
    return [[UIColor alloc] initWithRed:.75f green:1.0f blue:.75f alpha:1.0f];
  else
    return [[UIColor alloc] initWithRed:1.0f green:1.0f blue:.75f alpha:1.0f];
}

+(UIColor *) getEventTextColor:(KBKEvent *)event
{
  if ([event.title rangeOfString:@"Junior"].location != NSNotFound)
    return [[UIColor alloc] initWithRed:.1529f green:.5450f blue:.2627f alpha:1.0f];
  else if ([event.eFormat isEqualToString:@"Seminar"])
    return [[UIColor alloc] initWithRed:.4196f green:.0431f blue:.4078f alpha:1.0f];
  else if ([event.eFormat isEqualToString:@"SOG"] || [event.eFormat isEqualToString:@"MP Game"] || [event.title rangeOfString:@"Open Gaming"].location == 0)
    return [[UIColor alloc] initWithRed:.9529f green:.4392f blue:.1608f alpha:1.0f];
  else if ([event.eClass length] == 0)
    return [[UIColor alloc] initWithRed:.0f green:.2745 blue:.5333f alpha:1.0f];
  else if (event.qualify)
    return [[UIColor alloc] initWithRed:.9294f green:.1254 blue:.1418f alpha:1.0f];
  else
    return [UIColor blackColor];
}

+(UIFont *) getEventTextFont:(KBKEvent *)event
{
  int size = [UIFont systemFontSize];
  if ([event.title rangeOfString:@"Junior"].location != NSNotFound)
    return [UIFont systemFontOfSize:size];
  else if ([event.eFormat isEqualToString:@"Demo"] || [event.eClass length] == 0)
    return [UIFont italicSystemFontOfSize:size];
  else if (event.qualify)
    return [UIFont boldSystemFontOfSize:size];
  else
    return [UIFont systemFontOfSize:size];
}

@end
