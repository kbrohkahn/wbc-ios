//
//  KBKEventViewController.m
//  WBC
//
//  Created by Kevin on 7/12/14.
//  Copyright (c) 2014 Boardgame Players Association. All rights reserved.
//

#import "KBKEventViewController.h"
#import "KBKWBCData.h"
#import "KBKMapContainer.h"
#import "KBKTournament.h"

@interface KBKEventViewController ()

@end

@implementation KBKEventViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}

- (void)viewDidLoad
{
  [super viewDidLoad];
  
  [self.navigationItem setTitle:self.event.title];
  [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"BarButtonIconQuestion"] style:UIBarButtonItemStylePlain target:self action:@selector(openHelp)]];
  
  [self.background setBackgroundColor:[KBKWBCData getOddCellColor:self.event]];
  
  UIColor *textColor = [KBKWBCData getEventTextColor:self.event];
  UIFont *textFont = [KBKWBCData getEventTextFont:self.event];
  
  [self.dateAndTime setText:[[[KBKWBCData displayDayStrings] objectAtIndex:self.event.day] stringByAppendingString:[NSString stringWithFormat:@" %d00 to %d00", self.event.hour, self.event.hour + (int) self.event.totalDuration]]];
  [self.dateAndTime setTextColor:textColor];
  [self.dateAndTime setFont:textFont];
  
  self.location.color = textColor;
  [self.location setText:self.event.location];
  [self.location setTextColor:textColor];
  [self.location setFont:textFont];
  
  if ([self.event.eFormat length] == 0)
    [self.eFormat setHidden:YES];
  else {
    [self.eFormat setHidden:NO];
    [self.eFormat setText:[@"Format: " stringByAppendingString:self.event.eFormat]];
    [self.eFormat setTextColor:textColor];
    [self.eFormat setFont:textFont];
  }
  
  if ([self.event.eFormat length] == 0)
    [self.eClass setHidden:YES];
  else {
    [self.eClass setHidden:NO];
    [self.eClass setText:[@"Class: " stringByAppendingString:self.event.eClass]];
    [self.eClass setTextColor:textColor];
    [self.eClass setFont:textFont];
  }
  
  [self.eNote setText:self.event.note];
  [self.starred setBackgroundImage:[UIImage imageNamed:(self.event.starred ? @"StarOn" :@"StarOff")] forState:UIControlStateNormal];
  
  UITapGestureRecognizer *singleFingerTap =
  [[UITapGestureRecognizer alloc] initWithTarget:self
                                          action:@selector(didTapLabelWithGesture:)];
  [self.location addGestureRecognizer:singleFingerTap];
}

-(IBAction)openHelp
{
  UIViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"HelpViewController"];
  [self.navigationController pushViewController:newView animated:YES];
}

-(IBAction)share
{
  NSString *text = [NSString stringWithFormat:@"%@: %@", self.event.title, self.eNote.text];
  NSString *tournamentLabel =((KBKTournament *) [[KBKWBCData tournaments] objectAtIndex:self.event.tournamentID]).label;
  
  UIActivityViewController *controller;
  if ([tournamentLabel isEqualToString:@""]) {
    controller= [[UIActivityViewController alloc] initWithActivityItems:@[text] applicationActivities:nil];
  } else {
    NSURL *url = [NSURL URLWithString: [NSString stringWithFormat:@"http://boardgamers.org/yearbkex/%@pge.htm", tournamentLabel]];
    controller = [[UIActivityViewController alloc] initWithActivityItems:@[text, url] applicationActivities:nil];
  }
  
  
  controller.excludedActivityTypes = @[UIActivityTypePostToWeibo,
                                       UIActivityTypePrint,
                                       UIActivityTypeCopyToPasteboard,
                                       UIActivityTypeAssignToContact,
                                       UIActivityTypeSaveToCameraRoll,
                                       UIActivityTypeAddToReadingList,
                                       UIActivityTypePostToFlickr,
                                       UIActivityTypePostToVimeo,
                                       UIActivityTypePostToTencentWeibo,
                                       UIActivityTypeAirDrop];
  
  [self presentViewController:controller animated:YES completion:nil];
  
}

- (void)didTapLabelWithGesture:(UITapGestureRecognizer *)tapGesture {
  KBKMapContainer *map = [self.storyboard instantiateViewControllerWithIdentifier:@"MapContainer"];
  map.room = self.event.location;
  
  [self.navigationController pushViewController:map animated:YES];
}

-(void) viewWillDisappear:(BOOL)animated
{
  NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
  [defaults setValue:self.eNote.text forKey:[@"note_" stringByAppendingString:self.event.eventID]];
  [defaults synchronize];
  
  self.event.note = self.eNote.text;
}

- (void)didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (IBAction)buttonClicked:(id)sender {
  self.event.starred = !self.event.starred;
  [self.starred setBackgroundImage:[UIImage imageNamed:(self.event.starred ? @"StarOn" :@"StarOff")] forState:UIControlStateNormal];
  
  [KBKWBCData updateStarredEvent:self.event];
}
@end
