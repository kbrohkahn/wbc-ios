//
//  KBKMapViewController.m
//  WBC
//
//  Created by Kevin on 5/31/14.
//  Copyright (c) 2014 Boardgame Players Association. All rights reserved.
//

#import "KBKMapViewController.h"

@interface KBKMapViewController ()

@end

@implementation KBKMapViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}

- (void)viewDidLoad
{
  [super viewDidLoad];
  
  NSArray *roomsUpstairs = @[@"Conestoga 1", @"Conestoga 2", @"Conestoga 3", @"Good Spirits Bar", @"Heritage", @"Lampeter", @"Laurel Grove", @"Poolside Pavilion", @"Showroom", @"Vista CD", @"Wheatland"];
  NSArray *roomsUpstairsImages = @[@"room_conestoga_1", @"room_conestoga_2", @"room_conestoga_3", @"room_good_spirits_bar", @"room_heritage", @"room_lampeter", @"room_laurel_grove", @"room_open_gaming_pavilion", @"room_showroom", @"room_vistas_cd", @"room_wheatland"];
  
  NSArray *roomsDownstairs = @[@"B Corridor",
                               @"Ballroom A",@"Ballroom A & B",@"Ballroom B",@"Cornwall",@"Hopewell",@"Kinderhook",@"Limerock",@"Marietta",@"New Holland",@"Paradise",@"Strasburg",@"Terrace 1"
                               ,@"Terrace 2",@"Terrace 3",@"Terrace 4",@"Terrace 5",@"Terrace 6",@"Terrace 7"];
  NSArray *roomsDownstairsImages = @[@"room_ballroom_b_corridor", @"room_ballroom_a", @"room_ballrooms_a_and_b", @"room_ballroom_b", @"room_cornwall", @"room_hopewell", @"room_kinderhook", @"room_limerock", @"room_marietta", @"room_new_holland", @"room_paradise", @"room_strasburg", @"room_terrace", @"room_terrace", @"room_terrace", @"room_terrace", @"room_terrace", @"room_terrace", @"room_terrace"];
  
  for (int i=0; i<[roomsUpstairs count]; i++) {
    if ([_room isEqualToString:roomsUpstairs[i]]) {
      [_upstairsOverlay setImage:[UIImage imageNamed:roomsUpstairsImages[i]]];
      _upstairsRoom = YES;
      break;
    }
  }
  
  for (int i=0; i<[roomsDownstairs count]; i++) {
    if ([_room isEqualToString:roomsDownstairs[i]]) {
      [_downstairsOverlay setImage:[UIImage imageNamed:roomsDownstairsImages[i]]];
      _downstairsRoom = YES;
      break;
    }
  }
  
  if (_upstairsRoom || _downstairsRoom) {
    _repeatingTimer = [NSTimer scheduledTimerWithTimeInterval:0.5
                                                       target:self selector:@selector(changeOverlayVisible)
                                                     userInfo:nil repeats:YES];
  }
}

-(void) changeOverlayVisible
{
  if (_upstairsRoom) {
    [_upstairsOverlay setHidden:!_upstairsOverlay.hidden];
  } else if (_downstairsRoom) {
    [_downstairsOverlay setHidden:!_downstairsOverlay.hidden];
  }
}

- (void)didReceiveMemoryWarning
{
  
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

-(void) dealloc
{
  [self.repeatingTimer invalidate];
  
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
