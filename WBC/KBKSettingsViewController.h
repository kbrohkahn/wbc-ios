//
//  KBKSettingsViewController.h
//  WBC
//
//  Created by Kevin on 7/26/14.
//  Copyright (c) 2014 Boardgame Players Association. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KBKSettingsViewController : UIViewController
@property (strong, nonatomic) IBOutlet UISwitch *notifySwitch;

- (IBAction)notifyTimeChanged:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *notifyTimeLabel;
@property (strong, nonatomic) IBOutlet UIStepper *notifyTimeStepper;

@end
