//
//  KBKTournament.m
//  WBC
//
//  Created by Kevin on 7/8/14.
//  Copyright (c) 2014 Boardgame Players Association. All rights reserved.
//

#import "KBKTournament.h"

@implementation KBKTournament


-(id) initWithValues:(NSInteger) i title:(NSString *)t label:(NSString *) l isTournament:(BOOL) is prize:(int)p gm:(NSString*)g {
  self.tournamentID = i;
  self.title = t;
  self.label = l;
  self.isTournament = is;
  
  self.prize = p;
  self.gm = g;
  
  return self.init;
}

@end
