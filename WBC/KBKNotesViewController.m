//
//  KBKNotesViewController.m
//  WBC
//
//  Created by Kevin on 8/2/14.
//  Copyright (c) 2014 Boardgame Players Association. All rights reserved.
//

#import "KBKNotesViewController.h"
#import "KBKNoteTableViewCell.h"
#import "KBKWBCData.h"
#import "KBKEvent.h"
#import "KBKEventGroup.h"


@interface KBKNotesViewController ()

@end

@implementation KBKNotesViewController

- (void)viewDidLoad
{
  [super viewDidLoad];

  _notes = [[NSMutableArray alloc] init];
  KBKEvent *event;
  NSMutableArray *searchList;
  for (int i=0; i<[[KBKWBCData dayList] count]; i++) {
    for (int j=1; j<[[[KBKWBCData dayList] objectAtIndex:i] count]; j++) {

      searchList = ((KBKEventGroup *) [[[KBKWBCData dayList] objectAtIndex:i] objectAtIndex:j]).events;
      for (int k=0; k<[searchList count]; k++) {
        event = [searchList objectAtIndex:k];
        
        if ([event.note isEqualToString:@""]) {
          [_notes addObject:[NSString stringWithFormat:@"%@: %@", event.title, event.note]];
        }        
      }
    }
  }
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  return [_notes count];
}

-(UIView*) tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
  return nil;
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  NSString *note = [_notes objectAtIndex:indexPath.row];
  KBKNoteTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NoteTableCell"];
  [cell.noteTextView setText:note];
  
  return cell;
}

@end
