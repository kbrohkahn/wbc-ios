//
//  KBKFinishesViewController.h
//  WBC
//
//  Created by Kevin on 8/2/14.
//  Copyright (c) 2014 Boardgame Players Association. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KBKFinishesViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSMutableArray *finishes;

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end
