//
//  KBKEventGroup.m
//  WBC
//
//  Created by Kevin on 5/28/14.
//  Copyright (c) 2014 Boardgame Players Association. All rights reserved.
//

#import "KBKEventGroup.h"

@implementation KBKEventGroup


-(id) initWithValues: (NSString *)t events:(NSMutableArray *) e
{
  self.events = e;
  self.title = t;
  
  return self.init;
}

@end
