//
//  KBKAddEventController.m
//  WBC
//
//  Created by Kevin on 7/14/14.
//  Copyright (c) 2014 Boardgame Players Association. All rights reserved.
//

#import "KBKAddEventController.h"
#import "KBKWBCData.h"
#import "KBKEvent.h"
#import "KBKEventGroup.h"
#import "KBKUserEventsViewController.h"

@interface KBKAddEventController ()

@end

@implementation KBKAddEventController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}

- (void)viewDidLoad
{
  [super viewDidLoad];
  
  self.daySwitches = [[NSArray alloc] initWithObjects:_switchDay0,_switchDay1,_switchDay2,_switchDay3,_switchDay4,_switchDay5,_switchDay6,_switchDay7,_switchDay8, nil];
  
  [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(close)]];
  [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(addEvent)]];
  
  UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc] init];
  [keyboardDoneButtonView sizeToFit];
  UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                 style:UIBarButtonItemStyleBordered target:self
                                                                action:@selector(doneClicked:)];
  [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:doneButton, nil]];
  self.hourTextField.inputAccessoryView = keyboardDoneButtonView;
  self.durationTextField.inputAccessoryView = keyboardDoneButtonView;
}

- (IBAction)doneClicked:(id)sender
{
  NSLog(@"Done Clicked.");
  [self.view endEditing:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
  [textField resignFirstResponder];
  return NO;
}

-(IBAction)close
{
  [self.navigationController  popViewControllerAnimated:YES];
}

-(IBAction)addEvent
{
  NSString *title = self.titleTextField.text;
  int hour = [self.hourTextField.text intValue];
  NSInteger duration = [self.durationTextField.text doubleValue];
  NSString *location = self.locationTextField.text;
  
  if (hour > 100)
    hour = hour / 100;
  
  if (hour < 7 || hour > 24) {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Invalid hour"
                                                    message:@"Hour must be between 7 and 24, we apologize for any inconvenience."
                                                   delegate:nil
                                          cancelButtonTitle:@"Got it"
                                          otherButtonTitles:nil];
    [alert show];
    return;
  } else if ([title isEqualToString:@""]) {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No title"
                                                    message:@"Please enter an event title."
                                                   delegate:nil
                                          cancelButtonTitle:@"Got it"
                                          otherButtonTitles:nil];
    [alert show];
    return;
  } else if ([location isEqualToString:@""]) {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No location"
                                                    message:@"Please enter an event location."
                                                   delegate:nil
                                          cancelButtonTitle:@"Got it"
                                          otherButtonTitles:nil];
    [alert show];
    return;
  }
  
  
  KBKEvent *event, *temp;
  for (int i=0; i<[self.daySwitches count]; i++) {
    if ([self.daySwitches[i] isOn]) {
      NSString *id = [[NSString stringWithFormat:@"%d", i * 24 + hour] stringByAppendingString:title];
      
      event = [[KBKEvent alloc] initWithValues:id tournament:0 date:i hour:hour title:title class:@"" format:@"" qualify:NO duration:duration continuous:NO totalDuration:duration location:location];
      event.starred= YES;
      event.note = @"";
      
      [((KBKEventGroup *)[KBKWBCData dayList][i] [hour-6]).events insertObject:event atIndex:0];
      [KBKWBCData updateStarredEvent:event];
      
      // insert into tournament group
      NSInteger index = 0;
      for (;index<[[KBKUserEventsViewController events] count]; index++) {
        temp = [[KBKUserEventsViewController events] objectAtIndex:index];
        if (temp.day * 24 + temp.hour >i*24+hour || (temp.day * 24 + temp.hour ==i*24+hour && [temp.title compare:title]))
          break;
      }
      
      [[KBKUserEventsViewController events] insertObject:event atIndex:index];
    }
  }
  
  [self close];
}

- (void)didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
