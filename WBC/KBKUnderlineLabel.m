//
//  KBKUnderlineLabel.m
//  WBC
//
//  Created by Kevin on 6/4/14.
//  Copyright (c) 2014 Boardgame Players Association. All rights reserved.
//

#import "KBKUnderlineLabel.h"

@implementation KBKUnderlineLabel

- (id)initWithFrame:(CGRect)frame
{
  self = [super initWithFrame:frame];
  if (self) {
    // Initialization code
  }
  return self;
}


- (void)drawRect:(CGRect)rect {
  CGContextRef ctx = UIGraphicsGetCurrentContext();
  CGContextSetStrokeColorWithColor(ctx, [_color CGColor]);
  CGContextSetLineWidth(ctx, 1.0f);
  
  CGContextMoveToPoint(ctx, 0, self.bounds.size.height - 1);
  CGContextAddLineToPoint(ctx, self.bounds.size.width, self.bounds.size.height - 1);
  
  CGContextStrokePath(ctx);
  
  [super drawRect:rect];
}

@end
