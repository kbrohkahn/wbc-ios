//
//  KBKSearchTableViewController.m
//  WBC
//
//  Created by Kevin on 7/8/14.
//  Copyright (c) 2014 Boardgame Players Association. All rights reserved.
//

#import "KBKSearchTableViewController.h"
#import "KBKWBCData.h"
#import "KBKTournament.h"
#import "KBKTournamentViewController.h"

@interface KBKSearchTableViewController ()

@end

@implementation KBKSearchTableViewController


- (id)initWithStyle:(UITableViewStyle)style
{
  self = [super initWithStyle:style];
  if (self) {
    // Custom initialization
  }
  return self;
}

- (void)viewDidLoad
{
  [super viewDidLoad];
  
  _searchResults = [KBKWBCData tournaments];
  // Uncomment the following line to preserve selection between presentations.
  // self.clearsSelectionOnViewWillAppear = NO;
  
  // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
  // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
  // Return the number of sections.
  return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  if (tableView == self.searchDisplayController.searchResultsTableView) {
    return [self.searchResults count];
  } else {
    return [[KBKWBCData tournaments] count];
  }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  static NSString *CellIdentifier = @"SearchTableCell";
  UITableViewCell *cell = (UITableViewCell *)[self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
  
  // Configure the cell...
  if (cell == nil) {
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
  }
  
  // Display recipe in the table cell
  KBKTournament *tournament = nil;
  if (tableView == self.searchDisplayController.searchResultsTableView) {
    tournament = [self.searchResults objectAtIndex:indexPath.row];
  } else {
    tournament = ((KBKTournament *)[[KBKWBCData tournaments] objectAtIndex:indexPath.row]);
  }
  
  [cell.textLabel setText:tournament.title];
  return cell;
}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
  NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"title contains[c] %@", searchText];
  _searchResults = [[KBKWBCData tournaments] filteredArrayUsingPredicate:resultPredicate];
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
  [self filterContentForSearchText:searchString
                             scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                    objectAtIndex:[self.searchDisplayController.searchBar
                                                   selectedScopeButtonIndex]]];
  return YES;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  KBKTournament *tournament;
  if ([self.searchBar.text length] > 0) {
    tournament = ((KBKTournament *)[self.searchResults objectAtIndex:indexPath.row]);
  } else {
    tournament = ((KBKTournament *)[[KBKWBCData tournaments] objectAtIndex:indexPath.row]);
  }
  
  KBKTournamentViewController *destViewController = (KBKTournamentViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"TournamentViewController"];
  destViewController.tournamentID = tournament.tournamentID;
  
  [self.navigationController pushViewController:destViewController animated:YES];
  
}

@end
