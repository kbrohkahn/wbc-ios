//
//  KBKSchedulePageViewController.h
//  WBC
//
//  Created by Kevin on 5/29/14.
//  Copyright (c) 2014 Boardgame Players Association. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KBKScheduleViewController : UIViewController <UIPageViewControllerDataSource, UIActionSheetDelegate>

@property (strong, nonatomic) UIPageViewController *pageViewController;

@end
