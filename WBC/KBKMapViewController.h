//
//  KBKMapViewController.h
//  WBC
//
//  Created by Kevin on 5/31/14.
//  Copyright (c) 2014 Boardgame Players Association. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KBKMapViewController : UIViewController

@property (strong, nonatomic) NSString* room;
@property (nonatomic) BOOL upstairsRoom;
@property (nonatomic) BOOL downstairsRoom;

@property (strong, nonatomic) IBOutlet UIImageView *downstairsOverlay;
@property (strong, nonatomic) IBOutlet UIImageView *upstairsOverlay;

@property (weak) NSTimer *repeatingTimer;

@end
