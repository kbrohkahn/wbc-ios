//
//  KBKSettingsViewController.m
//  WBC
//
//  Created by Kevin on 7/26/14.
//  Copyright (c) 2014 Boardgame Players Association. All rights reserved.
//

#import "KBKSettingsViewController.h"
#import "KBKEventGroup.h"
#import "KBKWBCData.h"

@interface KBKSettingsViewController ()

@end

@implementation KBKSettingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}

- (void)viewDidLoad
{
  [super viewDidLoad];
  
  NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
  [self.notifySwitch setOn:[defaults boolForKey:@"notifyEvents"]];
  [self.notifyTimeStepper setValue:[defaults doubleForKey:@"notifyTime"]];
  [self.notifyTimeLabel setText:[NSString stringWithFormat:@"%1.0f minutes before start", [defaults doubleForKey:@"notifyTime"]]];
  
}

- (void)didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}
-(void) viewWillDisappear:(BOOL)animated
{
  NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
  [defaults setBool:[self.notifySwitch isOn] forKey:@"notifyEvents"];
  [defaults setDouble:[self.notifyTimeStepper value] forKey:@"notifyTime"];
  [defaults synchronize];
}

- (IBAction)notifyTimeChanged:(id)sender {
  [self.notifyTimeLabel setText:[NSString stringWithFormat:@"%1.0f minutes before start", [self.notifyTimeStepper value]]];
}
@end
