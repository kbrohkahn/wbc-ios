//
//  KBKSearchTableViewController.h
//  WBC
//
//  Created by Kevin on 7/8/14.
//  Copyright (c) 2014 Boardgame Players Association. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KBKSearchTableViewController : UITableViewController <UISearchBarDelegate, UISearchDisplayDelegate>

@property (strong, nonatomic) NSArray *searchResults;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;

@end
