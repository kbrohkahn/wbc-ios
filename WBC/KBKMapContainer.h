//
//  KBKMapContainer.h
//  WBC
//
//  Created by Kevin on 6/10/14.
//  Copyright (c) 2014 Boardgame Players Association. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KBKMapViewController.h"

@interface KBKMapContainer : UIViewController

@property (strong, nonatomic) KBKMapViewController *mapViewController;
@property (strong, nonatomic) NSString *room;

@end
