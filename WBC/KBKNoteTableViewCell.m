//
//  KBKNoteTableViewCell.m
//  WBC
//
//  Created by Kevin on 2/5/15.
//  Copyright (c) 2015 Boardgame Players Association. All rights reserved.
//

#import "KBKNoteTableViewCell.h"

@implementation KBKNoteTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
