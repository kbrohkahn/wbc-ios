//
//  KBKEventGroup.h
//  WBC
//
//  Created by Kevin on 5/28/14.
//  Copyright (c) 2014 Boardgame Players Association. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KBKEventGroup : NSObject

@property (strong, nonatomic) NSMutableArray *events;
@property (strong, nonatomic) NSString *title;

-(id) initWithValues: (NSString *)t events:(NSMutableArray *) e;

@end
