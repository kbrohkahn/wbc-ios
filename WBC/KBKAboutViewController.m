//
//  KBKAboutViewController.m
//  WBC
//
//  Created by Kevin on 7/13/14.
//  Copyright (c) 2014 Boardgame Players Association. All rights reserved.
//

#import "KBKAboutViewController.h"

@interface KBKAboutViewController ()

@end

@implementation KBKAboutViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}

- (void)viewDidLoad
{
  [super viewDidLoad];
  
  NSString *versionText = [@"App version: " stringByAppendingString:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]];
  
  NSError *error;
  NSString *sourceFile = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"schedule2014.txt"];
  NSDate *lastModif = [[[NSFileManager defaultManager] attributesOfItemAtPath:sourceFile error:&error  ] objectForKey:NSFileModificationDate];
  
  versionText = [versionText stringByAppendingString:@"\nLast update: "];
  
  NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
  [formatter setDateFormat:@"MMM d, yyyy"];
  
  versionText = [versionText stringByAppendingString:[formatter stringFromDate:lastModif]];
  
  [self.appVersionLabel setText:versionText];
}

- (void)didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
