//
//  KBKTournament.h
//  WBC
//
//  Created by Kevin on 7/8/14.
//  Copyright (c) 2014 Boardgame Players Association. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KBKTournament : NSObject

@property (nonatomic) NSInteger tournamentID;
@property (strong, nonatomic) NSString* title;
@property (strong, nonatomic) NSString *label;
@property (nonatomic) BOOL isTournament;

@property (nonatomic) NSInteger prize;
@property (strong, nonatomic) NSString *gm;

@property (nonatomic) NSInteger finish;
@property (nonatomic) BOOL visible;

-(id) initWithValues:(NSInteger) i title:(NSString *)t label:(NSString *) l isTournament:(BOOL) is prize:(int)p gm:(NSString*)g;

@end
