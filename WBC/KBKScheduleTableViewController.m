//
//  KBKViewController.m
//  WBC
//
//  Created by Kevin on 5/28/14.
//  Copyright (c) 2014 Boardgame Players Association. All rights reserved.
//

#import "KBKEvent.h"
#import "KBKEventGroup.h"
#import "KBKScheduleTableViewController.h"
#import "KBKScheduleTableViewCell.h"
#import "KBKTournamentViewController.h"
#import "KBKScheduleViewController.h"
#import "KBKMapContainer.h"
#import "KBKWBCData.h"
#import "KBKTournament.h"

@interface KBKScheduleTableViewController ()
@end

@implementation KBKScheduleTableViewController

-(void) viewDidAppear:(BOOL)animated
{
  [super viewDidAppear:animated];

  NSString *title = [[KBKWBCData displayDayStrings] objectAtIndex:_pageIndex];
  self.navigationController.navigationBar.topItem.title = [@"WBC " stringByAppendingString:title];
  
  [self.tableView reloadData];
  
  if (self.pageIndex == [KBKWBCData getDay]) {
    NSInteger hour = [KBKWBCData getHour] - 7;
    if (hour < 0)
      hour = 0;
    
    for (;hour < 19; hour++) {
      if ([((KBKEventGroup *)[self.eventGroups objectAtIndex:hour +1]).events count] > 0)
      {
        break;
      }
    }
    
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:hour + 1] atScrollPosition:UITableViewScrollPositionTop animated:YES];
  }
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
  return [_eventGroups count];
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  KBKEventGroup *eg =[_eventGroups objectAtIndex:section];
  return [eg.events count];
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  if (indexPath.row == 0)
    NSLog(@"Index path row is zero");
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
  static NSString *simpleTableIdentifier = @"ScheduleTableGroup";
  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
  
  if (cell == nil)
  {
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
  }
  
  
  NSString *title =((KBKEventGroup *) [_eventGroups objectAtIndex:section]).title;
  title = [title stringByAppendingString:@": "];
  
  if (section > 0) {
    BOOL started, finished;
    NSMutableArray *starredEvents;
    KBKEvent *event;
    for (int i=0; i<= _pageIndex; i++) {
      starredEvents = ((KBKEventGroup *) [[KBKWBCData dayList][i] objectAtIndex:0]).events;
      for (int j=0; j<[starredEvents count]; j++) {
        event = starredEvents[j];
        finished = event.day * 24 + event.hour +event.totalDuration <= _pageIndex * 24 + section + 6;
        started = event.day * 24 + event.hour <= _pageIndex * 24 + section + 6;
        
        if (started && !finished)  {
          title = [title stringByAppendingString:event.title];
          title = [title stringByAppendingString:@", "];
        }
      }
    }
  }
  
  cell.textLabel.text =[title substringToIndex:[title length] -2];
  
  [cell setBackgroundColor:[UIColor colorWithRed:1 green:0 blue:0 alpha:1]];
  
  return cell;
  
}
- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
  return 32.0f;
}

- (CGFloat) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
  return 0.001f;
  
}

-(UIView*) tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
  return nil;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
  KBKEvent *e = [((KBKEventGroup *) [_eventGroups objectAtIndex:indexPath.section]).events objectAtIndex:indexPath.row];
  
  if (((KBKTournament*)[[KBKWBCData tournaments] objectAtIndex:e.tournamentID]).visible || e.starred)
    return 36;
  else
    return 0;
  
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  KBKEvent *event = [((KBKEventGroup *) [_eventGroups objectAtIndex:indexPath.section]).events objectAtIndex:indexPath.row];
  
  NSString *simpleTableIdentifier;
  if (((KBKTournament*)[[KBKWBCData tournaments] objectAtIndex:event.tournamentID]).visible || event.starred)
    simpleTableIdentifier=@"ScheduleTableCell";
  else
    simpleTableIdentifier=@"InvisibleTableCell";
  
  KBKScheduleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
  if (cell == nil)
    cell = [[KBKScheduleTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
  if ([simpleTableIdentifier isEqualToString:@"InvisibleTableCell"])
    return cell;
  
  UIColor *textColor = [KBKWBCData getEventTextColor:event];
  UIFont *textFont = [KBKWBCData getEventTextFont:event];
  
  // title
  NSString *title;
  if (indexPath.section == 0)
  {
    title = [[NSNumber numberWithInteger:event.hour] stringValue];
    title = [title stringByAppendingString:@" - "];
    title = [title stringByAppendingString:event.title];
  } else
    title = event.title;
  
  cell.titleLabel.text = title;
  [cell.titleLabel setTextColor:textColor];
  [cell.titleLabel setFont:textFont];
  
  /*
   if ([title rangeOfString:@"Junior"].location != NSNotFound) {
   [cell.titleLabel setFrame:CGRectMake(0, 0, cell.titleLabel.bounds.size.width, cell.titleLabel.bounds.size.height)];
   UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(2, 12, 20, 12)];
   [image setImage:[UIImage imageNamed:@"JuniorIcon"]];
   [cell.titleLabel addSubview:image];
   }
   */
  
  // location
  cell.locationLabel.color = textColor;
  cell.locationLabel.text = event.location;
  [cell.locationLabel setTextColor:textColor];
  [cell.locationLabel setFont:textFont];

  UITapGestureRecognizer *singleFingerTap =
  [[UITapGestureRecognizer alloc] initWithTarget:self
                                          action:@selector(didTapLabelWithGesture:)];
  [cell.locationLabel addGestureRecognizer:singleFingerTap];

  // duration
  if (event.continuous)
    cell.durationLabel.text = [NSString stringWithFormat:@"%2.2f C", event.duration];
  else
    cell.durationLabel.text = [NSString stringWithFormat:@"%2.2f", event.duration];
  [cell.durationLabel setTextColor:textColor];
  [cell.durationLabel setFont:textFont];
  
  // star
  cell.starButton.tag = indexPath.section * 1000 + indexPath.row;
  [cell.starButton setBackgroundImage:[UIImage imageNamed:(event.starred?@"StarOn":@"StarOff")] forState:UIControlStateNormal];
  
  // background
  if ([cell isSelected])
    [cell setBackgroundColor:[[UIColor alloc] initWithRed:.9365f green:.75f blue:.75f alpha:1.0f]];
  else if ([cell isHighlighted])
    [cell setBackgroundColor:[[UIColor alloc] initWithRed:.0f green:.5f blue:1.0f alpha:1.0f]];
  else if (indexPath.row % 2 == 0)
    [cell setBackgroundColor:[KBKWBCData getOddCellColor:event]];
  else
    [cell setBackgroundColor:[KBKWBCData getEvenCellColor:event]];
  
  return cell;
}

- (void)didTapLabelWithGesture:(UITapGestureRecognizer *)tapGesture {
  NSString * room = ((UILabel*)tapGesture.view).text;
  KBKMapContainer *map = [self.storyboard instantiateViewControllerWithIdentifier:@"MapContainer"];
  map.room = room;
  
  [self.navigationController pushViewController:map animated:YES];
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
  if ([segue.identifier isEqualToString:@"ScheduleToTournament"])
  {
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    KBKEvent *e =((KBKEvent *)[((KBKEventGroup *) [_eventGroups objectAtIndex:indexPath.section]).events objectAtIndex:indexPath.row]);
    
    KBKTournamentViewController *destViewController = segue.destinationViewController;
    destViewController.tournamentID = e.tournamentID;
  }
}

- (IBAction)changeEventStar:(id)sender {
  NSInteger tag = ((UIButton *) sender).tag;
  
  KBKEvent *e = [((KBKEventGroup *) [_eventGroups objectAtIndex:tag/1000]).events objectAtIndex:tag%1000];
  e.starred = !e.starred;
  [KBKWBCData updateStarredEvent:e];
  [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

@end
