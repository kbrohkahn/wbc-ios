//
//  KBKEventViewController.h
//  WBC
//
//  Created by Kevin on 7/12/14.
//  Copyright (c) 2014 Boardgame Players Association. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KBKEvent.h"
#import "KBKUnderlineLabel.h"

@interface KBKEventViewController : UIViewController

@property (strong, nonatomic) KBKEvent *event;
@property (strong, nonatomic) IBOutlet UIView *background;
@property (strong, nonatomic) IBOutlet UILabel *dateAndTime;
@property (strong, nonatomic) IBOutlet KBKUnderlineLabel *location;
@property (strong, nonatomic) IBOutlet UILabel *eFormat;
@property (strong, nonatomic) IBOutlet UILabel *eClass;
@property (strong, nonatomic) IBOutlet UIButton *starred;

- (IBAction)buttonClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *eNote;

@end
