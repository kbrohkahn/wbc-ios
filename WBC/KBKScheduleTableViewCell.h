//
//  KBKScheduleTableViewCell.h
//  WBC
//
//  Created by Kevin on 5/28/14.
//  Copyright (c) 2014 Boardgame Players Association. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KBKUnderlineLabel.h"

@interface KBKScheduleTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIButton *starButton;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *durationLabel;
@property (strong, nonatomic) IBOutlet KBKUnderlineLabel *locationLabel;

@end
