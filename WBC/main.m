//
//  main.m
//  WBC
//
//  Created by Kevin on 5/28/14.
//  Copyright (c) 2014 Boardgame Players Association. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "KBKAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([KBKAppDelegate class]));
    }
}
